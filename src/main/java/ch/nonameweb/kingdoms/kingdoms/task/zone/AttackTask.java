package ch.nonameweb.kingdoms.kingdoms.task.zone;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.title.Title;

import java.util.function.Consumer;

public class AttackTask implements Consumer<Task> {

    private Zone zone;
    private Kingdom attacker;
    private Kingdom defender;

    private FlagTask flagTask;

    private Task task;

    public AttackTask(Zone zone, Kingdom attacker, Kingdom defender ) {

        this.zone = zone;
        this.attacker = attacker;
        this.defender = defender;

    }

    @Override
    public void accept(Task task) {

        this.task = task;

        if ( zone.isAttack() ) {
            startAttack();
        } else {
            stopAttack();
            task.cancel();
        }

    }

    private void startAttack() {

        zone.setAttacker(attacker);
        zone.setDefender(defender);

        attacker.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "Ihr könnt nun die Zone (", zone.getName(), ").")));
        defender.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "Die Zone (", zone.getName(), ") kann nun Angegriffen werden.")));

        // Start Zone Task
        flagTask = new FlagTask(zone, attacker, defender, this, task);
        Kingdoms.getInstance().getTaskManager().createSyncRepeatingTask(flagTask, 0L, 1L);

    }

    public void stopAttack() {

        // Winner?
        if ( flagTask.wasAttackSuccessfully() ) {
            zone.setKingdom( attacker.getId() );

            attacker.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "Der Angriff ist vorbei!"), Text.of(TextColors.GREEN, "Dein Königreich hat die Zonne eingenommen.")));
            defender.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "Der Angriff ist vorbei!"), Text.of(TextColors.RED, "Dein Königreich hat die Zone verlohren.")));
        } else {

            attacker.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "Der Angriff ist vorbei!"), Text.of(TextColors.GREEN, "Dein Königreich konnte die Zone nicht einnehmen.")));
            defender.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "Der Angriff ist vorbei!"), Text.of(TextColors.RED, "Dein Königreich hat die Zone verteidigt.")));

        }

        // Start Cooldown Task
        zone.setAttackCooldown(true);

        Long delay = (long)zone.getCooldown();

        Consumer<Task> task = new CooldownTask(zone);
        Kingdoms.getInstance().getTaskManager().createAsyncDelayedTask(task, delay);

        // Reset Zone
        Kingdoms.getInstance().getZoneManager().resetZone(zone);
    }

}