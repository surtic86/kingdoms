package ch.nonameweb.kingdoms.kingdoms.channels;

import ch.nonameweb.kingdoms.kingdoms.Config;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class KingdomStaffMessageChannel extends BaseMessageChannel {

    public KingdomStaffMessageChannel() {
        setPrefix(Text.of(TextColors.DARK_GREEN, Config.confNode.getNode("Settings", "Chat", "KingdomStaff", "Prefix").getString()));
    }

}
