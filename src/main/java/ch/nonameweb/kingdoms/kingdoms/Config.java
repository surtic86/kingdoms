package ch.nonameweb.kingdoms.kingdoms;

import ch.nonameweb.kingdoms.kingdoms.entity.KingdomLevel;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

import java.io.File;
import java.io.IOException;

public class Config {

    public static File config;
    public static ConfigurationLoader<CommentedConfigurationNode> loader;
    public static CommentedConfigurationNode confNode;


    public static void setup(File file, ConfigurationLoader<CommentedConfigurationNode> load) {
        config = file;
        loader = load;
    }

    public static void load() {

        try {

            if (!config.exists()) {

                config.createNewFile();
                confNode = loader.load();
                addDefaultValues();
                loader.save(confNode);
            }

            confNode = loader.load();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void save() {
        try {
            loader.save(confNode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void addDefaultValues() {

        confNode.getNode("Settings", "MySQL", "Host").setValue("localhost");
        confNode.getNode("Settings", "MySQL", "Port").setValue("3306");
        confNode.getNode("Settings", "MySQL", "DB").setValue("Database");
        confNode.getNode("Settings", "MySQL", "User").setValue("User");
        confNode.getNode("Settings", "MySQL", "Password").setValue("Password");

        confNode.getNode("Settings", "Kingdoms", "Distance", "MinKingdoms").setValue(550);
        confNode.getNode("Settings", "Kingdoms", "Distance", "MinZones").setValue(350);
        confNode.getNode("Settings", "Kingdoms", "Distance", "MinSpawn").setValue(600);

        confNode.getNode("Settings", "Kingdoms", "Base", "MinPlayers").setValue(1);

        confNode.getNode("Settings", "Zones", "Distance", "MaxSpawn").setValue(100);


        confNode.getNode("Settings", "Teleport", "Delay").setValue(5);
        confNode.getNode("Settings", "Teleport", "Cooldown").setValue(60);

        confNode.getNode("Settings", "Chat", "Local", "MaxDistance").setValue(80);
        confNode.getNode("Settings", "Chat", "Local", "Prefix").setValue("[Local]");
        confNode.getNode("Settings", "Chat", "Kingdom", "Prefix").setValue("[Kingdom]");
        confNode.getNode("Settings", "Chat", "KingdomStaff", "Prefix").setValue("[KingdomStaff]");
        confNode.getNode("Settings", "Chat", "Global", "Prefix").setValue("[Global]");
        confNode.getNode("Settings", "Chat", "Staff", "Prefix").setValue("[Staff]");

        confNode.getNode("Settings", "Task", "Save", "Time").setValue(360);


        confNode.getNode("Settings", "Kingdoms", "MaxLevel").setValue(5);

        confNode.getNode("Settings", "Kingdoms", "Level", 1, "Size").setValue(20);
        confNode.getNode("Settings", "Kingdoms", "Level", 1, "Points").setValue(0);
        confNode.getNode("Settings", "Kingdoms", "Level", 1, "Members").setValue(0);

        confNode.getNode("Settings", "Kingdoms", "Level", 2, "Size").setValue(40);
        confNode.getNode("Settings", "Kingdoms", "Level", 2, "Points").setValue(20);
        confNode.getNode("Settings", "Kingdoms", "Level", 2, "Members").setValue(1);

        confNode.getNode("Settings", "Kingdoms", "Level", 3, "Size").setValue(60);
        confNode.getNode("Settings", "Kingdoms", "Level", 3, "Points").setValue(30);
        confNode.getNode("Settings", "Kingdoms", "Level", 3, "Members").setValue(1);

        confNode.getNode("Settings", "Kingdoms", "Level", 4, "Size").setValue(80);
        confNode.getNode("Settings", "Kingdoms", "Level", 4, "Points").setValue(40);
        confNode.getNode("Settings", "Kingdoms", "Level", 4, "Members").setValue(2);

        confNode.getNode("Settings", "Kingdoms", "Level", 5, "Size").setValue(100);
        confNode.getNode("Settings", "Kingdoms", "Level", 5, "Points").setValue(50);
        confNode.getNode("Settings", "Kingdoms", "Level", 5, "Members").setValue(4);
    }

    public static KingdomLevel getLevel(Integer level) {

        KingdomLevel kingdomLevel = new KingdomLevel();
        kingdomLevel.setLevel(level);
        kingdomLevel.setMembers(confNode.getNode("Settings", "Kingdoms", "Level", ""+level, "Members").getInt());
        kingdomLevel.setPoints(confNode.getNode("Settings", "Kingdoms", "Level", ""+level, "Points").getInt());
        kingdomLevel.setSize(confNode.getNode("Settings", "Kingdoms", "Level", ""+level, "Size").getInt());

        return kingdomLevel;
    }

}
