package ch.nonameweb.kingdoms.kingdoms.entity;

public enum KingdomChannel {
    STAFF,
    GLOBAL,
    KINGDOM,
    KINGDOM_STAFF,
    LOCAL,
}
