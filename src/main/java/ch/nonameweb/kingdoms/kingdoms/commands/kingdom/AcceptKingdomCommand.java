package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class AcceptKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        srcKingdom = getKingdomManager().getKingdomByInvitedPlayer(srcPlayer);

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "You are not Invited in any Kingdom!") );
            return CommandResult.success();
        }

        srcMember.setRank( KingdomRank.NOVICE );
        srcMember.setKingdom( srcKingdom.getId() );

        srcKingdom.removeInvitedMember(srcMember);
        srcKingdom.addMember(srcMember);

        srcPlayer.sendMessage( Text.of(TextColors.GOLD, "You have joined the Kingdom!") );
        srcKingdom.sendMessageToMembers(Text.of(TextColors.GOLD, srcPlayer.getName(), " has joined the Kingdom!"));

        srcKingdom.addToMessageChannels(srcMember, srcPlayer);

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.accept")
                .executor(new AcceptKingdomCommand())
                .build();
    }

}