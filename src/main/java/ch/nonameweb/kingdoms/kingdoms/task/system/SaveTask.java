package ch.nonameweb.kingdoms.kingdoms.task.system;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import org.spongepowered.api.scheduler.Task;

import java.util.function.Consumer;

public class SaveTask implements Consumer<Task> {

    @Override
    public void accept(Task task) {

        Kingdoms.getInstance().getLogger().info("Kingdoms Plugin: SaveTask saving....");
        Kingdoms.getInstance().getMemberManager().saveAll();
        Kingdoms.getInstance().getKingdomManager().saveAll();
        Kingdoms.getInstance().getPlotManager().saveAll();
        Kingdoms.getInstance().getZoneManager().saveAll();
        Kingdoms.getInstance().getLogger().info("Kingdoms Plugin: SaveTask saved");

    }
}
