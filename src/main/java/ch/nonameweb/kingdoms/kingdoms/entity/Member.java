package ch.nonameweb.kingdoms.kingdoms.entity;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.Utils;
import ch.nonameweb.kingdoms.kingdoms.selection.Selection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.api.scoreboard.critieria.Criteria;
import org.spongepowered.api.scoreboard.displayslot.DisplaySlots;
import org.spongepowered.api.scoreboard.objective.Objective;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@DatabaseTable(tableName = "member")
public class Member {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private UUID uuid;

    @DatabaseField
    private int kingdom = 0;

    @DatabaseField
    private int money = 0;

    @DatabaseField
    private boolean online = false;


    @DatabaseField
    private String channel = "LOCAL";

    @DatabaseField
    private String rank = "NONE";

    // Login's
    @DatabaseField
    private String firstLogin = "";
    @DatabaseField
    private String lastLogin = "";
    @DatabaseField
    private String lastLogout = "";

    // Stats
    @DatabaseField
    private int playerKills = 0;
    @DatabaseField
    private int playerDeaths = 0;
    @DatabaseField
    private int monsterKills = 0;
    @DatabaseField
    private int monsterDeaths = 0;
    @DatabaseField
    private int blockBreak = 0;
    @DatabaseField
    private int blockPlace = 0;
    @DatabaseField
    private int playtime = 0;
    @DatabaseField
    private int ironCollected = 0;
    @DatabaseField
    private int goldCollected = 0;
    @DatabaseField
    private int diamondCollected = 0;

    private Selection selection = new Selection();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getUniqueId() {
        return uuid;
    }

    public void setUniqueId(UUID uuid) {
        this.uuid = uuid;
    }

    public String getKingdomName() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom.getName();
        }

        return null;
    }

    public String getKingdomTag() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom.getTag();
        }

        return null;
    }

    public Kingdom getKingdom() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom;
        }

        return null;
    }

    public int getKingdomId() {
        return kingdom;
    }

    public void setKingdom(int kingdom) {
        this.kingdom = kingdom;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public KingdomChannel getChannel() {
        return KingdomChannel.valueOf( this.channel );
    }

    public void setChannel(KingdomChannel channel) {
        this.channel = channel.name();
    }

    public KingdomRank getRank() {
        return KingdomRank.valueOf( this.rank );
    }

    public void setRank(KingdomRank rank) {
        this.rank = rank.name();
    }

    public int getPlayerKills() {
        return playerKills;
    }

    public void addPlayerKill() {
        this.playerKills++;
    }

    public void setPlayerKills(int playerKills) {
        this.playerKills = playerKills;
    }

    public int getPlayerDeaths() {
        return playerDeaths;
    }

    public void addPlayerDeath() {
        this.playerDeaths++;
    }

    public void setPlayerDeaths(int playerDeaths) {
        this.playerDeaths = playerDeaths;
    }

    public int getMonsterKills() {
        return monsterKills;
    }

    public void addMonsterKill() {
        this.monsterKills++;
    }

    public void setMonsterKills(int monsterKills) {
        this.monsterKills = monsterKills;
    }

    public int getMonsterDeaths() {
        return monsterDeaths;
    }

    public void addMonsterDeath() {
        this.monsterDeaths++;
    }

    public void setMonsterDeaths(int monsterDeaths) {
        this.monsterDeaths = monsterDeaths;
    }

    public int getBlockBreak() {
        return blockBreak;
    }

    public void addBlockBreak() {
        this.blockBreak++;
    }

    public void setBlockBreak(int blockBreak) {
        this.blockBreak = blockBreak;
    }

    public int getBlockPlace() {
        return blockPlace;
    }

    public void addBlockPlace() {
        this.blockPlace++;
    }

    public void setBlockPlace(int blockPlace) {
        this.blockPlace = blockPlace;
    }

    public Date getLastLogin() {

        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastLogin);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public void setLastLogout(Date date) {
        this.lastLogout = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);

        calculatePlaytime();
    }

    public Date getLastLogout() {

        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(lastLogout);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public void setLastLogin(Date date) {
        this.lastLogin = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public Date getFirstLogin() {

        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(firstLogin);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public void setFirstLogin(Date date) {
        this.firstLogin = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public int getMoney() {
        return money;
    }

    public void addMoney( int amount ) {
        this.money += amount;
    }

    public void removeMoney( int amount ) {
        this.money -= amount;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getPlaytime() {
        return this.playtime;
    }

    public void addPlaytime(int time) {
        this.playtime += time;
    }

    private void calculatePlaytime() {

        long diff = getLastLogout().getTime() - getLastLogin().getTime();
        int seconds = (int)diff / 1000;

        addPlaytime( seconds );
    }

    public void setPlaytime(int playtime) {
        this.playtime = playtime;
    }

    public void updateMessageChannel() {
        Player player = Sponge.getServer().getPlayer(getUniqueId()).orElseGet(null);

        if (getChannel() == KingdomChannel.LOCAL) {
            player.setMessageChannel(Kingdoms.getInstance().getLocalMessageChannel());

        } else if (getChannel() == KingdomChannel.GLOBAL) {
            player.setMessageChannel(Kingdoms.getInstance().getGlobalChannel());

        } else if (getChannel() == KingdomChannel.STAFF) {
            player.setMessageChannel(Kingdoms.getInstance().getStaffMessageChannel());

        } else if (getChannel() == KingdomChannel.KINGDOM) {
            player.setMessageChannel(getKingdom().getMessageChannel());

        } else if (getChannel() == KingdomChannel.KINGDOM_STAFF) {
            player.setMessageChannel(getKingdom().getStaffMessageChannel());
        }

    }

    public Selection getSelection() {
        return selection;
    }

    public String getName() {
        return Utils.getUserName(getUniqueId());
    }

    public int getIronCollected() {
        return ironCollected;
    }

    public void setIronCollected(int ironCollected) {
        this.ironCollected = ironCollected;
    }

    public void addIronCollected() {
        ironCollected++;
    }

    public int getGoldCollected() {
        return goldCollected;
    }

    public void setGoldCollected(int goldCollected) {
        this.goldCollected = goldCollected;
    }

    public void addGoldCollected() {
        goldCollected++;
    }

    public int getDiamondCollected() {
        return diamondCollected;
    }

    public void setDiamondCollected(int diamondCollected) {
        this.diamondCollected = diamondCollected;
    }

    public void addDiamondCollected() {
        diamondCollected++;
    }

    public Player getPlayer() {
        return Sponge.getServer().getPlayer(uuid).orElseGet(null);
    }

    public void setPlayerKingdomTag() {

        Player player = getPlayer();

        if (player == null || getKingdomId() == 0) {
            return;
        }

        Scoreboard sb = Scoreboard.builder().build();
        Objective obj = Objective.builder().name(player.getName()).displayName(Text.of(TextColors.GOLD, "[", TextColors.WHITE, getKingdomTag(), TextColors.GOLD, "]")).criterion(Criteria.DUMMY).build();
        sb.addObjective(obj);
        sb.updateDisplaySlot(obj, DisplaySlots.BELOW_NAME);
        player.setScoreboard(sb);
    }


}

