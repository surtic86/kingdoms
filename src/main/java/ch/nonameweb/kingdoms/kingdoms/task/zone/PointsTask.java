package ch.nonameweb.kingdoms.kingdoms.task.zone;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import ch.nonameweb.kingdoms.kingdoms.entity.ZoneType;
import org.spongepowered.api.scheduler.Task;

import java.util.ArrayList;
import java.util.function.Consumer;

public class PointsTask implements Consumer<Task> {

    @Override
    public void accept(Task task) {

        ArrayList<Zone> zones = Kingdoms.getInstance().getZoneManager().getAllZones();

        if ( zones != null ) {

            for ( Zone zone : zones ) {

                if ( zone.isActive() ) {

                    Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomByName( zone.getKingdomName() );

                    if ( kingdom != null ) {

                        if (zone.getType().equals(ZoneType.MILITARY)) {
                            kingdom.addMilitaryPoints( zone.getPoints() );

                        } else if (zone.getType().equals(ZoneType.MINE)) {
                            kingdom.addMinePoints( zone.getPoints() );

                        } else if (zone.getType().equals(ZoneType.CIVIL)) {
                            kingdom.addCivilPoints( zone.getPoints() );

                        } else {
                            kingdom.addPoints( zone.getPoints() );
                        }

                    }

                }

            }
        }

    }
}
