package ch.nonameweb.kingdoms.kingdoms.task.zone;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.world.World;

import java.util.function.Consumer;

public class FlagTask implements Consumer<Task> {

    private Zone zone;
    private Kingdom attacker;
    private Kingdom defender;

    private Integer lastAttackers = 0;
    private Integer lastDefenders = 0;

    private Integer flagStateOld = 10;

    private Integer flagCaptureTime = 0;

    private AttackTask attackTask;
    private Task task;

    private World world;

    public FlagTask( Zone zone, Kingdom attacker, Kingdom defender, AttackTask attackTask, Task task ) {

        this.zone = zone;
        this.attacker = attacker;
        this.defender = defender;
        this.attackTask = attackTask;
        this.task = task;

        world = Sponge.getServer().getWorld("world").orElseGet(null);

    }

    @Override
    public void accept(Task task) {

        if ( !zone.isAttack() ) {
            task.cancel();
        }

        int attackers = 0;
        int defenders = 0;

        if ( Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( zone, attacker ) != null ) {
            attackers = Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( zone, attacker ).size();
        }

        if ( Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( zone, defender ) != null ) {
            defenders = Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( zone, defender ).size();
        }

        if ( attackers > defenders && lastAttackers > lastDefenders ) {

            if ( flagStateOld > 0 ) {

                // Update Flag
                Kingdoms.getInstance().getZoneManager().updateFlag( world, zone, flagStateOld, -1 );

                flagStateOld--;

            } else {

                if (flagCaptureTime == zone.getCaptureTime()) {
                    zone.setAttack(false);

                    attackTask.stopAttack();
                    task.cancel();

                } else {
                    flagCaptureTime++;
                }

            }


        } else if ( attackers < defenders && lastAttackers < lastDefenders ) {

            if ( flagStateOld < 10 ) {

                // Update Flag
                Kingdoms.getInstance().getZoneManager().updateFlag( world, zone, flagStateOld, +1 );

                flagStateOld++;

                flagCaptureTime = 0;
            }

        }

        if (zone.isAttack()) {
            zone.sendFlageStateTitle(flagStateOld);
            zone.addScoreboard();
        } else {
            zone.removeScoreboard();
        }

        lastAttackers = attackers;
        lastDefenders = defenders;

    }

    public boolean wasAttackSuccessfully() {

        if ( flagStateOld == 0 ) {
            return true;
        }

        return false;
    }

}