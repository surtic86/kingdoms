package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SetMinPlayersZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();
        String type = args.<String>getOne(Text.of("type")).get();
        Integer value = args.<Integer>getOne(Text.of("value")).get();

        Zone zone = getZoneManager().getZoneByName( name );

        if ( zone == null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is no Zone with this Name!"));
            return CommandResult.success();
        }

        if ( type.equals("attackers")) {
            zone.setMinAttackers(value);
        } else if ( type.equals("defenders") ) {
            zone.setMinDefenders(value);
        } else {
            src.sendMessage(Text.of(TextColors.RED, "/zone setminplayers <name> <attackers|defenders> <count>"));
            return CommandResult.success();
        }

        src.sendMessage(Text.of(TextColors.GOLD, "Zone Property is set!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.setminplayers")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.string(Text.of("type")),
                        GenericArguments.integer(Text.of("value"))
                )
                .executor(new SetMinPlayersZoneCommand())
                .build();
    }

}