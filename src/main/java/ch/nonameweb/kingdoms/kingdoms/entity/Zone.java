package ch.nonameweb.kingdoms.kingdoms.entity;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scoreboard.Scoreboard;
import org.spongepowered.api.scoreboard.critieria.Criteria;
import org.spongepowered.api.scoreboard.displayslot.DisplaySlots;
import org.spongepowered.api.scoreboard.objective.Objective;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.title.Title;
import org.spongepowered.api.world.Location;

import java.util.ArrayList;

@DatabaseTable(tableName = "zone")
public class Zone {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;
    @DatabaseField
    private boolean active = false;
    @DatabaseField
    private boolean build = false;


    // Owner
    @DatabaseField
    private int kingdom = 0;


    // Corner One
    @DatabaseField
    private int cornerOneX;
    @DatabaseField
    private int cornerOneY;
    @DatabaseField
    private int cornerOneZ;


    // Corner Two
    @DatabaseField
    private int cornerTwoX;
    @DatabaseField
    private int cornerTwoY;
    @DatabaseField
    private int cornerTwoZ;


    // Flag
    @DatabaseField
    private int flagX = 0;
    @DatabaseField
    private int flagY = 0;
    @DatabaseField
    private int flagZ = 0;


    // Settings
    @DatabaseField
    private String type = "NONE";
    @DatabaseField
    private int cost = 0;
    @DatabaseField
    private int points = 0;
    @DatabaseField
    private int lifePoolDefenders = 0;
    @DatabaseField
    private int lifePoolAttackers = 0;
    @DatabaseField
    private int minDefenders = 0;
    @DatabaseField
    private int minAttackers = 0;
    @DatabaseField
    private int delay = 0;
    @DatabaseField
    private int duration = 0;
    @DatabaseField
    private int cooldown = 0;
    @DatabaseField
    private int captureTime = 0;


    // Spawn Points
    @DatabaseField
    private int spawnDefendersX = 0;
    @DatabaseField
    private int spawnDefendersY = 0;
    @DatabaseField
    private int spawnDefendersZ = 0;

    @DatabaseField
    private int spawnAttackersX = 0;
    @DatabaseField
    private int spawnAttackersY = 0;
    @DatabaseField
    private int spawnAttackersZ = 0;



    // Other Stuff
    private boolean attack = false;
    private boolean attackCooldown = false;

    private Kingdom attacker = null;
    private Kingdom defender = null;

    private int deathCounterAttacker = 0;
    private int deathCounterDefenders = 0;

    private int flagLvl = 0;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(ZoneType type) {
        this.type = type.name();
    }

    public ZoneType getType() {
        return ZoneType.valueOf(this.type);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isBuild() {
        return build;
    }

    public void setBuild(boolean build) {
        this.build = build;
    }

    public String getKingdomName() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom.getName();
        }

        return null;
    }

    public Kingdom getKingdom() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom;
        }

        return null;
    }

    public int getKingdomId() {
        return kingdom;
    }


    public void setKingdom(int kingdom) {
        this.kingdom = kingdom;
    }

    public int getCornerOneX() {
        return cornerOneX;
    }

    public void setCornerOneX(int cornerOneX) {
        this.cornerOneX = cornerOneX;
    }

    public int getCornerOneY() {
        return cornerOneY;
    }

    public void setCornerOneY(int cornerOneY) {
        this.cornerOneY = cornerOneY;
    }

    public int getCornerOneZ() {
        return cornerOneZ;
    }

    public void setCornerOneZ(int cornerOneZ) {
        this.cornerOneZ = cornerOneZ;
    }

    public Location getCornerOne() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getCornerOneX(), getCornerOneY(), getCornerOneZ());
    }

    public int getCornerTwoX() {
        return cornerTwoX;
    }

    public void setCornerTwoX(int cornerTwoX) {
        this.cornerTwoX = cornerTwoX;
    }

    public int getCornerTwoY() {
        return cornerTwoY;
    }

    public void setCornerTwoY(int cornerTwoY) {
        this.cornerTwoY = cornerTwoY;
    }

    public int getCornerTwoZ() {
        return cornerTwoZ;
    }

    public void setCornerTwoZ(int cornerTwoZ) {
        this.cornerTwoZ = cornerTwoZ;
    }

    public Location getCornerTwo() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getCornerTwoX(), getCornerTwoY(), getCornerTwoZ());
    }

    public int getFlagX() {
        return flagX;
    }

    public void setFlagX(int flagX) {
        this.flagX = flagX;
    }

    public int getFlagY() {
        return flagY;
    }

    public void setFlagY(int flagY) {
        this.flagY = flagY;
    }

    public int getFlagZ() {
        return flagZ;
    }

    public void setFlagZ(int flagZ) {
        this.flagZ = flagZ;
    }

    public Location getFlag() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getFlagX(), getFlagY(), getFlagZ());
    }

    public void setFlag(Location location) {
        setFlagX(location.getBlockX());
        setFlagY(location.getBlockY() - 1);
        setFlagZ(location.getBlockZ());
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getLifePoolDefenders() {
        return lifePoolDefenders;
    }

    public void setLifePoolDefenders(int lifePoolDefenders) {
        this.lifePoolDefenders = lifePoolDefenders;
    }

    public int getLifePoolAttackers() {
        return lifePoolAttackers;
    }

    public void setLifePoolAttackers(int lifePoolAttackers) {
        this.lifePoolAttackers = lifePoolAttackers;
    }

    public int getMinDefenders() {
        return minDefenders;
    }

    public void setMinDefenders(int minDefenders) {
        this.minDefenders = minDefenders;
    }

    public int getMinAttackers() {
        return minAttackers;
    }

    public void setMinAttackers(int minAttackers) {
        this.minAttackers = minAttackers;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getCooldown() {
        return cooldown;
    }

    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }


    public void setSpwanDefenders(Location location) {
        spawnDefendersX = location.getBlockX();
        spawnDefendersY = location.getBlockY();
        spawnDefendersZ = location.getBlockZ();
    }

    public int getSpawnDefendersX() {
        return spawnDefendersX;
    }

    public int getSpawnDefendersY() {
        return spawnDefendersY;
    }

    public int getSpawnDefendersZ() {
        return spawnDefendersZ;
    }

    public Location getSpawnDefenders() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getSpawnDefendersX(), getSpawnDefendersY(), getSpawnDefendersZ());
    }

    public void setSpwanAttackers(Location location) {
        spawnAttackersX = location.getBlockX();
        spawnAttackersY = location.getBlockY();
        spawnAttackersZ = location.getBlockZ();
    }

    public int getSpawnAttackersX() {
        return spawnAttackersX;
    }

    public int getSpawnAttackersY() {
        return spawnAttackersY;
    }

    public int getSpawnAttackersZ() {
        return spawnAttackersZ;
    }

    public Location getSpawnAttackers() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getSpawnAttackersX(), getSpawnAttackersY(), getSpawnAttackersZ());
    }

    public boolean isAttack() {
        return attack;
    }

    public void setAttack(boolean attack) {
        this.attack = attack;
    }

    public boolean getAttackCooldown() {
        return attackCooldown;
    }

    public void setAttackCooldown(boolean attackCooldown) {
        this.attackCooldown = attackCooldown;
    }

    public Kingdom getAttacker() {
        return attacker;
    }

    public void setAttacker(Kingdom attacker) {
        this.attacker = attacker;
    }

    public Kingdom getDefender() {
        return defender;
    }

    public void setDefender(Kingdom defender) {
        this.defender = defender;
    }

    public int getDeathCounterAttacker() {
        return deathCounterAttacker;
    }

    public void addDeathCounterAttacker() {
        this.deathCounterAttacker++;
    }

    public void setDeathCounterAttacker(int deathCounterAttacker) {
        this.deathCounterAttacker = deathCounterAttacker;
    }

    public int getDeathCounterDefenders() {
        return deathCounterDefenders;
    }

    public void addDeathCounterDefenders() {
        this.deathCounterAttacker++;
    }

    public void setDeathCounterDefenders(int deathCounterDefenders) {
        this.deathCounterDefenders = deathCounterDefenders;
    }

    public int getFlagLvl() {
        return flagLvl;
    }

    public void setFlagLvl(int flagLvl) {
        this.flagLvl = flagLvl;
    }

    public boolean contains(Location location) {

        if ( getCornerTwoX() > location.getBlockX() && getCornerOneX() < location.getBlockX()) {

            if ( getCornerTwoZ() > location.getBlockZ() && getCornerOneZ() < location.getBlockZ() ) {
                return true;
            }

        }

        return false;
    }

    public boolean isNearFlag( Location location ) {

        if ( getFlag().getPosition().distance( location.getPosition() ) < 10 ) {
            return true;
        }

        return false;
    }

    public int getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(int captureTime) {
        this.captureTime = captureTime;
    }


    public int getLeftRespawnsAttackers() {
        return lifePoolAttackers - deathCounterAttacker;
    }

    public int getLeftRespawnsDefenders() {
        return lifePoolDefenders - deathCounterDefenders;
    }

    public ArrayList<Player> getAllPlayers() {

        ArrayList<Player> players = new ArrayList<>();

        for (Member member : getAttacker().getOnlineMembers()) {
            players.add(Sponge.getServer().getPlayer(member.getUniqueId()).get());
        }

        for (Member member : getDefender().getOnlineMembers()) {
            players.add(Sponge.getServer().getPlayer(member.getUniqueId()).get());
        }

        return players;

    }

    public void addScoreboard() {
        int attackers = 0;
        int defenders = 0;

        if ( Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( this, attacker ) != null ) {
            attackers = Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( this, attacker ).size();
        }

        if ( Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( this, defender ) != null ) {
            defenders = Kingdoms.getInstance().getZoneManager().getOnlinePlayersFromKingdomNearZoneFlag( this, defender ).size();
        }

        Scoreboard sb = Scoreboard.builder().build();
        Objective obj = Objective.builder().criterion(Criteria.DUMMY).displayName(Text.of(TextColors.GOLD, getName())).name("MainSideBoard").build();

        obj.getOrCreateScore(Text.of(TextColors.BLACK, "-----------------", "   ")).setScore(30);
        obj.getOrCreateScore(Text.of(TextColors.GREEN, "Spieler bei der Flagge")).setScore(29);
        obj.getOrCreateScore(Text.of(TextColors.BLACK, "-----------------", "  ")).setScore(28);
        obj.getOrCreateScore(Text.of("Angreifer: ", attackers)).setScore(27);
        obj.getOrCreateScore(Text.of("Verteidiger: ", defenders)).setScore(26);
        obj.getOrCreateScore(Text.of(TextColors.BLACK, "-----------------", "   ")).setScore(25);

        obj.getOrCreateScore(Text.of(TextColors.GREEN, "Verbleibende Respawns")).setScore(24);
        obj.getOrCreateScore(Text.of(TextColors.BLACK, "-----------------", "    ")).setScore(23);
        obj.getOrCreateScore(Text.of("Angreifer: ", getLeftRespawnsAttackers())).setScore(22);
        obj.getOrCreateScore(Text.of("Verteidiger: ", getLeftRespawnsDefenders())).setScore(21);
        obj.getOrCreateScore(Text.of(TextColors.BLACK, "-----------------", "     ")).setScore(20);

        sb.addObjective(obj);
        sb.updateDisplaySlot(obj, DisplaySlots.SIDEBAR);


        for (Player player : getAllPlayers()) {
            player.getScoreboard().getScores().clear();
            player.setScoreboard(sb);
        }

    }

    public void removeScoreboard() {

        for (Player player : getAllPlayers()) {
            player.getScoreboard().getScores().clear();
        }

    }

    public void sendFlageStateTitle(int state) {

        Text text = Text.of("");

        for (int i = 0; i < 10; i++) {

            if (i < state) {
                text = text.concat(Text.of(TextColors.RED, "|"));
            } else {
                text = text.concat(Text.of(TextColors.GREEN, "|"));
            }

        }

        for (Player player : getAllPlayers()) {
            player.sendTitle(Title.of(text));
        }

    }

}

