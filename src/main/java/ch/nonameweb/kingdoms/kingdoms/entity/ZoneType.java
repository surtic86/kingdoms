package ch.nonameweb.kingdoms.kingdoms.entity;

public enum ZoneType {
    MILITARY,
    MINE,
    CIVIL,
    NONE
}