package ch.nonameweb.kingdoms.kingdoms.commands.plot;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Plot;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ListPlotCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "List all Plots"));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        for ( Plot plot : getPlotManager().getPlotsByKingdom(srcMember.getKingdom()) ) {

            String size = "Size : " + (plot.getCornerTwoX() - plot.getCornerOneX()) + " x " + (plot.getCornerTwoZ() - plot.getCornerOneZ()) + " / " + (plot.getCornerTwoY() - plot.getCornerOneY());

            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Name : ", TextColors.WHITE, plot.getName(), TextColors.YELLOW, " Owner : ", TextColors.WHITE, plot.getOwnerName()));
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, size));

            srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        }

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.plot.list")
                .executor(new ListPlotCommand())
                .build();
    }

}