package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.Config;
import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import ch.nonameweb.kingdoms.kingdoms.task.kingdom.SpawnTask;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SpawnZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();

        Zone zone = getZoneManager().getZoneByName( name );

        if ( zone == null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is no Zone with this Name!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if ( zone.getKingdomId() != srcKingdom.getId() ) {
            src.sendMessage(Text.of(TextColors.RED, "The Zone ist nor yours!"));
            return CommandResult.success();
        }

        Integer teleport_delay = Config.confNode.getNode("Settings", "Teleport", "Delay").getInt();

        getTaskManager().createSyncDelayedTask(new SpawnTask(srcPlayer, zone.getSpawnDefenders()), teleport_delay);

        srcPlayer.sendMessage(Text.of(TextColors.RED, "You will be Teleportet in ", teleport_delay, " sec! Don't move!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.spwan")
                .arguments(
                        GenericArguments.string(Text.of("name"))
                )
                .executor(new SpawnZoneCommand())
                .build();
    }

}