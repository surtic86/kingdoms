package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

public class SetFlagZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        Zone zone = getZoneManager().getZoneByLocation(srcPlayer.getLocation());

        if ( zone == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You need to be in a Zone!"));
            return CommandResult.success();
        }

        // Create new Flag
        World world = srcPlayer.getWorld();


        // Reset old Flag
        if ( zone.getFlagX() != 0 && zone.getFlagY() != 0 && zone.getFlagZ() != 0 ) {
            getZoneManager().removeFlag(world, zone);
        }

        // Set new Flag
        zone.setFlag(srcPlayer.getLocation());

        getZoneManager().createFlag(world, zone);

        src.sendMessage(Text.of(TextColors.GOLD, "Flag is created!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.create")
                .executor(new SetFlagZoneCommand())
                .build();
    }

}