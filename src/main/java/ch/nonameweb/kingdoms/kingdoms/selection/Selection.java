package ch.nonameweb.kingdoms.kingdoms.selection;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class Selection {

    private Location location1 = null;
    private Location location2 = null;

    public Location getLocation1() {
        return location1;
    }

    public void setLocation1(Location location1) {
        this.location1 = location1;
    }

    public Location getLocation2() {
        return location2;
    }

    public void setLocation2(Location location2) {
        this.location2 = location2;
    }

    public World getWorld() {
        return (World) location1.getExtent();
    }

    public Location getMinLocation() {
        return new Location(getWorld(), Math.min(location1.getX(), location2.getX()), Math.min(location1.getY(), location2.getY()), Math.min(location1.getZ(), location2.getZ()));
    }

    public Location getMaxLocation() {
        return new Location(getWorld(), Math.max(location1.getX(), location2.getX()), Math.max(location1.getY(), location2.getY()), Math.max(location1.getZ(), location2.getZ()));
    }


    public void handleInteractBlockEvent(InteractBlockEvent event, Player player, Boolean primary) {

        BlockSnapshot targetBlock = event.getTargetBlock();

        if (targetBlock.getState().getType().equals(BlockTypes.AIR) ) {
            return;
        }

        if (!player.hasPermission("kingdoms.zones.selection") ) {
            return; // silently
        }

        ItemStack itemStack = player.getItemInHand(HandTypes.MAIN_HAND).get();

        if (!itemStack.getType().equals(ItemTypes.WOODEN_AXE) ) {
            return;
        }

        Optional<Location<World>> optionalWorldLocation = targetBlock.getLocation();

        if (!optionalWorldLocation.isPresent()) {
            return;
        }

        Location location = optionalWorldLocation.get();

        String corner;

        if (primary) {
            corner = "1";
            setLocation1(location);
        } else {
            corner = "2";
            setLocation2(location);
        }

        player.sendMessage(Text.of(TextColors.RED, "Point ", corner, ": X : ", location.getBlockX(), "  Y : ", location.getBlockY(), " Z : ", location.getBlockZ()));

    }

}

