package ch.nonameweb.kingdoms.kingdoms.channels;

import ch.nonameweb.kingdoms.kingdoms.Config;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class KingdomMessageChannel extends BaseMessageChannel {

    public KingdomMessageChannel() {
        setPrefix(Text.of(TextColors.GREEN, Config.confNode.getNode("Settings", "Chat", "Kingdom", "Prefix").getString()));
    }
}
