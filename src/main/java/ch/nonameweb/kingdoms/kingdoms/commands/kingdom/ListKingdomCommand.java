package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ListKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "List all Kingdoms"));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "TAG - NAME - POINTS - MILITARY - MINE - CIVIL - ALL POINT - Players "));

        // TODO Sort the Kingdoms by Points All / Points, Spacers
        for ( Kingdom kingdom : getKingdomManager().getAllKingdoms()  ) {
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, kingdom.getTag()+" - "+kingdom.getName()+" - "+kingdom.getPoints()+" - "+kingdom.getMilitaryPoints()+" - "+kingdom.getMinePoints()+" - "+kingdom.getCivilPoints()+" - "+kingdom.getPointsAll()+" - "+kingdom.getMembers().size()));
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.list")
                .executor(new ListKingdomCommand())
                .build();
    }

}