package ch.nonameweb.kingdoms.kingdoms.manager;


import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.spongepowered.api.entity.living.player.Player;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

public class MemberManager {

    private Kingdoms plugin;

    private ConnectionSource connectionSource;
    private Dao<Member, String> dao;

    private ArrayList<Member> members;

    public MemberManager() {
        plugin = Kingdoms.getInstance();
        connectionSource = plugin.getConnectionSource();

        try {
            dao = DaoManager.createDao(connectionSource, Member.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        members = new ArrayList<Member>();

        load();
    }

    /**
     * Load all Member Entries
     */
    private void load() {

        try {
            TableUtils.createTableIfNotExists(connectionSource, Member.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            members = (ArrayList)dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        plugin.getLogger().info("loaded Members : " + members.size());

    }

    /**
     * Save a Member Entry over Player
     *
     * @param player
     */
    public void save( Player player) {

        if ( members.size() > 0 ) {

            for ( Member member : members ) {

                if ( member.getUniqueId().equals(player.getUniqueId()) ) {
                    try {
                        dao.createOrUpdate(member);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }

        }

    }

    /**
     * Save all Member Entries
     */
    public void saveAll() {

        if ( members.size() > 0 ) {

            for ( Member member : members ) {
                try {
                    dao.createOrUpdate(member);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    /**
     * Load a Member Entry over the Player
     *
     * @param player
     * @return
     */
    public Member getMemberByPlayer( Player player ) {

        if ( members.size() > 0 ) {

            for ( Member member : members ) {

                if ( member.getUniqueId().equals( player.getUniqueId() ) ) {
                    return member;
                }

            }

        }

        return null;
    }

    public Member getMemberById(Integer id) {

        if ( members.size() > 0 ) {

            for ( Member member : members ) {

                if ( member.getId() == id ) {
                    return member;
                }

            }

        }

        return null;
    }

    public ArrayList<Member> getMembers() {
        return members;
    }

    /**
     * Create a new Member Entry
     *
     * @param player
     * @return
     */
    public Member createMember( Player player ) {

        Member member = new Member();
        member.setUniqueId( player.getUniqueId() );
        member.setFirstLogin( new Date( System.currentTimeMillis() ) );

        members.add(member);
        save(player);

        return member;
    }

}

