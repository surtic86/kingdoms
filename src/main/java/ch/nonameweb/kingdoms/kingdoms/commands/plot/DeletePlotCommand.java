package ch.nonameweb.kingdoms.kingdoms.commands.plot;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Plot;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class DeletePlotCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!") );
            return CommandResult.success();
        }


        Plot plot = getPlotManager().getPlotByLocation(srcPlayer.getLocation());

        if ( plot == null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is no Plot at this Place!"));
            return CommandResult.success();
        }

        getPlotManager().deletePlot(plot);

        src.sendMessage(Text.of(TextColors.GOLD, "Plot was deleted"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.plot.info")
                .executor(new DeletePlotCommand())
                .build();
    }

}