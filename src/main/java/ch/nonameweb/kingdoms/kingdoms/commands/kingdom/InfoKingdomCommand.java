package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;

public class InfoKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        Kingdom kingdom = null;

        if ( args.hasAny("name") ) {

            String name = args.<String>getOne("name").get();

            kingdom = getKingdomManager().getKingdomByName( name );

            if ( kingdom == null ) {
                kingdom = getKingdomManager().getKingdomByTag( name );
            }

        } else {
            kingdom = srcKingdom;
        }

        if ( kingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "There is no Kingdom with this Name!"));
            return CommandResult.success();
        }

        ArrayList<String> leaders = getKingdomManager().getMembersNameFromKingdomByRank(kingdom, KingdomRank.LEADER);
        ArrayList<String> captains = getKingdomManager().getMembersNameFromKingdomByRank(kingdom, KingdomRank.CAPTAIN);
        ArrayList<String> members = getKingdomManager().getMembersNameFromKingdomByRank(kingdom, KingdomRank.MEMBER);
        ArrayList<String> novices = getKingdomManager().getMembersNameFromKingdomByRank(kingdom, KingdomRank.NOVICE);

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Info [" + kingdom.getTag() + "] " + kingdom.getName()));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Level : ", TextColors.GOLD, kingdom.getLevel().getLevel(), TextColors.WHITE, " Points All : ", TextColors.GOLD, kingdom.getPointsAll()));
        srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Points Military : ", TextColors.GOLD, kingdom.getMilitaryPoints(), TextColors.WHITE, " Points Mine : ", TextColors.GOLD, kingdom.getMinePoints(), TextColors.WHITE, " Points Civil : ", TextColors.GOLD, kingdom.getCivilPoints(), TextColors.WHITE, " Points : ", TextColors.GOLD, kingdom.getPoints()));

        srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Player Kills : ", TextColors.GOLD, kingdom.getPlayerKills(), TextColors.WHITE, " Player Deaths : ", TextColors.GOLD, kingdom.getPlayerDeaths()));
        srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Monster Kills : ", TextColors.GOLD, kingdom.getMonsterKills(), TextColors.WHITE, " Monster Deaths : ", TextColors.GOLD, kingdom.getMonsterDeaths()));
        srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Block places : ", TextColors.GOLD, kingdom.getBlockPlace(), TextColors.WHITE, " Block breaks : ", TextColors.GOLD, kingdom.getBlockBreak()));

        if ( leaders != null ) {
            srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Leaders : ", TextColors.GOLD, leaders.toString()));
        }

        if ( captains != null ) {
            srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Captains : ", TextColors.GOLD, captains.toString()));
        }

        if ( members != null ) {
            srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Members : ", TextColors.GOLD, members.toString()));
        }

        if ( novices != null ) {
            srcPlayer.sendMessage(Text.of(TextColors.WHITE, "Novices : ", TextColors.GOLD, novices.toString()));
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.info")
                .arguments(
                        GenericArguments.optional(GenericArguments.string(Text.of("name")))
                )
                .executor(new InfoKingdomCommand())
                .build();
    }

}