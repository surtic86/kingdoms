package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class PromoteKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!"));
            return CommandResult.success();
        }

        Player promotePlayer = args.<Player>getOne("promotePlayer").get();
        Member promoteMember = getMemberManager().getMemberByPlayer(promotePlayer);

        if ( promoteMember == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There is no Player with this Name!"));
            return CommandResult.success();
        }

        if ( !srcMember.getKingdomName().equalsIgnoreCase( promoteMember.getKingdomName() ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "The Player is not in your Kingdom!"));
            return CommandResult.success();
        }

        if ( promoteMember.getRank().equals( KingdomRank.LEADER ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't promote a Leader!"));
            return CommandResult.success();
        }

        if ( srcMember.getRank().equals( KingdomRank.CAPTAIN ) ) {

            if ( promoteMember.getRank().equals( KingdomRank.CAPTAIN ) ) {
                srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't promote a Member who has the same Rank!"));
                return CommandResult.success();
            }

        }

        if ( promoteMember.getRank().equals( KingdomRank.NOVICE ) ) {
            promoteMember.setRank( KingdomRank.MEMBER );

        } else if ( promoteMember.getRank().equals( KingdomRank.MEMBER ) ) {
            promoteMember.setRank( KingdomRank.CAPTAIN );

        } else if ( promoteMember.getRank().equals( KingdomRank.CAPTAIN ) ) {
            promoteMember.setRank( KingdomRank.LEADER );
        }

        srcKingdom.sendMessageToMembers(Text.of(TextColors.GOLD ,promotePlayer.getName(), " was promoted to ", promoteMember.getRank()));

        if (promotePlayer.isOnline()) {
            promotePlayer.sendMessage( Text.of(TextColors.GOLD , "You where promoted to ", promoteMember.getRank()) );
        }

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.promote")
                .arguments(
                        GenericArguments.player(Text.of("promotePlayer"))
                )
                .executor(new PromoteKingdomCommand())
                .build();
    }
}
