package ch.nonameweb.kingdoms.kingdoms.commands.plot;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;

public class CreatePlotCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You are not a Member of a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            src.sendMessage(Text.of(TextColors.RED, "You need to be a Captain oder Leader to do this!"));
            return CommandResult.success();
        }


        String name = args.<String>getOne(Text.of("name")).get();

        if (name.isEmpty()) {
            src.sendMessage(Text.of(TextColors.RED, "/plot create <name>"));
            return CommandResult.success();
        }


        if ( srcMember.getSelection().getLocation1() == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You need to select the first Corner!"));
            return CommandResult.success();
        }

        if ( srcMember.getSelection().getLocation2() == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You need to select the second Corner!"));
            return CommandResult.success();
        }

        Location cornerOne = srcMember.getSelection().getMinLocation();
        Location cornerTwo = srcMember.getSelection().getMaxLocation();

        if (!srcKingdom.contains(cornerOne)) {
            src.sendMessage(Text.of(TextColors.RED, "The first Corner ist not in the Kingdom!"));
            return CommandResult.success();
        }

        if (!srcKingdom.contains(cornerTwo)) {
            src.sendMessage(Text.of(TextColors.RED, "The second Corner ist not in the Kingdom!"));
            return CommandResult.success();
        }

        if ( getPlotManager().getPlotFromKingdomByLocation(srcKingdom, cornerOne) != null ) {
            src.sendMessage(Text.of(TextColors.RED, "The first Corner ist in a other Plot!"));
            return CommandResult.success();
        }

        if ( getPlotManager().getPlotFromKingdomByLocation(srcKingdom, cornerTwo) != null ) {
            src.sendMessage(Text.of(TextColors.RED, "The second Corner ist in a other Plot!"));
            return CommandResult.success();
        }

        getPlotManager().createPlot(srcKingdom, name, cornerOne, cornerTwo);
        src.sendMessage(Text.of(TextColors.GOLD, "The Plot is created!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.plot.create")
                .arguments(
                        GenericArguments.string(Text.of("name"))
                )
                .executor(new CreatePlotCommand())
                .build();
    }

}