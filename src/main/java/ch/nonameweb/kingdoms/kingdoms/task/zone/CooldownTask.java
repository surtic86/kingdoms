package ch.nonameweb.kingdoms.kingdoms.task.zone;

import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.scheduler.Task;

import java.util.function.Consumer;

public class CooldownTask implements Consumer<Task> {
    private Zone zone;

    public CooldownTask( Zone zone ) {
        this.zone = zone;
    }

    @Override
    public void accept(Task task) {
        zone.setAttackCooldown(false);
        task.cancel();
    }
}
