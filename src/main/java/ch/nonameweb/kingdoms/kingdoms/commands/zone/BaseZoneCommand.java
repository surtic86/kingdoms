package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class BaseZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Help"));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));


        if ( srcPlayer.hasPermission("kingdoms.command.zone.list") ) {
            srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone list -> lists all Zones"));
        }

        if ( srcPlayer.hasPermission("kingdoms.command.zone.info") ) {
            srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone info <name> -> Infos about Zone"));
        }

        if ( srcPlayer.hasPermission("kingdoms.command.zone.spawn") ) {
            srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone spawn <name> -> Teleports you to the Zone"));
        }


        if ( srcPlayer.hasPermission("kingdoms.command.zone.create") ) {

            if ( srcPlayer.hasPermission("kingdoms.command.zone.create") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone create <name> -> create a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setbuild") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setbuild <name> <true|false> -> Set the build Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setactive") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setactive <name> <true|false> -> Set the active Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setcost") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setcost <name> <cost> -> Set the cost Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setlifepool") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setlifepool <name> <attackers|defenders> <lives> -> Set the lifepool Property for Defenders or Attackers of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setminplayers") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setminplayers <name> <attackers|defenders> <count> -> Set the minplayers Property for Defenders or Attackers of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setdelay") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setdelay <name> <cost> -> Set the delay Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setpoints") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setdelay <name> <points> -> Set the points Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setduration") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setduration <name> <duration> -> Set the duration Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setcooldown") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setcooldown <name> <cooldown> -> Set the cooldown Property of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setkingdom") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setkingdom <name> <kingdom> -> Set the Kingdom of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.settype") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone settype <name> <military|mine|civil> -> Set the Type of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setspwan") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setspawn <name> <attackers|defenders> -> Set the Spawn for Defenders or Attackers of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setflag") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setflag -> Sets the Flag of a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.delete") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone delete <name> -> delete a Zone"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.zone.setcapturetime") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone setcapturetime <name> <time> -> set Flag Capture Time of a Zone"));
            }

        }

        if (srcKingdom != null) {

            if ( srcPlayer.hasPermission("kingdoms.command.zone.attack") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "zone attack <name> -> attack a Zone"));
            }

        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone")
                .child(SpawnZoneCommand.build(), "spawn")
                .child(CreateZoneCommand.build(), "create")
                .child(ListZoneCommand.build(), "list")
                .child(SetBuildZoneCommand.build(), "setbuild")
                .child(SetActiveZoneCommand.build(), "setactive")
                .child(SetCostZoneCommand.build(), "setcost")
                .child(SetLifePoolZoneCommand.build(), "setlifepool")
                .child(SetDelayZoneCommand.build(), "setdelay")
                .child(InfoZoneCommand.build(), "info")
                .child(SetPointsZoneCommand.build(), "setpoints")
                .child(SetKingdomZoneCommand.build(), "setkingdom")
                .child(SetDurationZoneCommand.build(), "setduration")
                .child(SetCooldownZoneCommand.build(), "setcooldown")
                .child(SetMinPlayersZoneCommand.build(), "setminplayers")
                .child(DeleteZoneCommand.build(), "delete")
                .child(SetSpawnZoneCommand.build(), "setspawn")
                .child(SetFlagZoneCommand.build(), "setflag")
                .child(SetTypeZoneCommand.build(), "settype")
                .child(AttackZoneCommand.build(), "attack")
                .child(SetCaptureTimePoolZoneCommand.build(), "setcapturetime", "sct")
                .executor(new BaseZoneCommand())
                .build();
    }

}
