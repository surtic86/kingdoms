package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.Config;
import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;

public class CreateZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();

        Location location1 = srcMember.getSelection().getLocation1();
        Location location2 = srcMember.getSelection().getLocation2();

        if ( location1 == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You need to select the first Corner!"));
            return CommandResult.success();
        }

        if ( location2 == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You need to select the second Corner!"));

            return CommandResult.success();
        }

        Integer distance_min_zone = Config.confNode.getNode("Settings", "Kingdoms", "Distance", "MinZones").getInt();


        if ( getKingdomManager().getAllKingdomsNearLocation( location1, distance_min_zone ) != null ) {

            // TODO better Info System about the near to a Kingdom
            src.sendMessage(Text.of(TextColors.RED, "Point 1 is to near to a Kingdom!"));
            return CommandResult.success();
        }

        if ( getKingdomManager().getAllKingdomsNearLocation( location2, distance_min_zone ) != null ) {

            // TODO better Info System about the near to a Kingdom
            src.sendMessage(Text.of(TextColors.RED, "Point 2 is to near to a Kingdom!"));
            return CommandResult.success();
        }

        if ( getZoneManager().getZoneByName( name ) != null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is already a Zone with this Name!"));
            return CommandResult.success();
        }

        if ( getZoneManager().getZoneByLocation( location1 ) != null ) {

            src.sendMessage(Text.of(TextColors.RED, "Point 1 is in a other Zone!"));
            return CommandResult.success();
        }

        if ( getZoneManager().getZoneByLocation( location2 ) != null ) {
            src.sendMessage(Text.of(TextColors.RED, "Point 2 is in a other Zone!"));
            return CommandResult.success();
        }

        getZoneManager().createZone(name, srcMember.getSelection().getMinLocation(), srcMember.getSelection().getMaxLocation());
        src.sendMessage(Text.of(TextColors.GOLD, "The Zone is created!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.create")
                .arguments(
                        GenericArguments.string(Text.of("name"))
                )
                .executor(new CreateZoneCommand())
                .build();
    }

}