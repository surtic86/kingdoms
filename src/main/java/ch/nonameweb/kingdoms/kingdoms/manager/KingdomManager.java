package ch.nonameweb.kingdoms.kingdoms.manager;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;

import java.sql.SQLException;
import java.util.ArrayList;

public class KingdomManager {

    private Kingdoms plugin;

    private ConnectionSource connectionSource;
    private Dao<Kingdom, String> dao;

    private MemberManager memberManager;
    //private PlotManager plotManager;

    private ArrayList<Kingdom> kingdoms;

    public KingdomManager() {
        plugin = Kingdoms.getInstance();
        connectionSource = plugin.getConnectionSource();

        try {
            dao = DaoManager.createDao(connectionSource, Kingdom.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        memberManager = plugin.getMemberManager();
        //plotManager = plugin.getPlotManager();

        kingdoms = new ArrayList<Kingdom>();

        load();
    }

    /**
     * Load all Kingdom Entries
     */
    public void load() {

        try {
            TableUtils.createTableIfNotExists(connectionSource, Kingdom.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            kingdoms = (ArrayList) dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                for ( Member member : memberManager.getMembers() ) {

                    if ( kingdom.getId() == member.getKingdomId() ) {

                        kingdom.addMember(member);

                    }

                }

            }

        }

        plugin.getLogger().info("loaded Kingdoms : " + kingdoms.size());

    }

    /**
     * Save all Kingdom Entries
     */
    public void saveAll() {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                try {
                    dao.createOrUpdate(kingdom);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    /**
     * Create a Kingdom
     *
     * @param member
     * @param name
     * @param tag
     */
    public void createKingdom(Member member, String name, String tag ) {

        Kingdom kingdom = new Kingdom();
        kingdom.setName( name );
        kingdom.setTag( tag );
        kingdom.addMember( member );
        kingdom.setLevel(1);

        try {
            dao.create(kingdom);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        kingdoms.add(kingdom);

        member.setKingdom( kingdom.getId() );
    }

    /**
     * Delete the Kingdom with all his stuff
     *
     * @param kingdom
     */
    public void deleteKingdom( Kingdom kingdom ) {

        // Reset Zones
        // TODO reset all Zones

        // Delete Plots
//        if ( plotManager.getPlotsByKingdom(kingdom).size() > 0 ) {
//
//            for ( Plot plot : plotManager.getPlotsByKingdom(kingdom) ) {
//                plotManager.deletePlot(plot);
//            }
//
//        }

        // Delete Kingdom
        kingdoms.remove(kingdom);
        try {
            dao.delete(kingdom);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public Kingdom getKingdomById( Integer id) {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                if ( kingdom.getId() == id ) {
                    return kingdom;
                }

            }

        }

        return null;
    }

    /**
     *
     *
     * @param player
     * @return
     */
    public Kingdom getKingdomByPlayer( Player player) {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                for ( Member member : kingdom.getMembers() ) {

                    if ( member.getUniqueId().equals( player.getUniqueId() ) ) {

                        return kingdom;

                    }

                }

            }

        }

        return null;
    }

    public Kingdom getKingdomByName( String name) {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                if ( kingdom.getName().equalsIgnoreCase( name ) ) {
                    return kingdom;
                }

            }

        }

        return null;
    }

    public Kingdom getKingdomByTag( String tag) {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                if ( kingdom.getTag().equalsIgnoreCase( tag ) ) {
                    return kingdom;
                }

            }

        }

        return null;
    }

    /**
     *
     *
     * @return
     */
    public ArrayList<Kingdom> getAllKingdoms() {
        return kingdoms;
    }

    public ArrayList<Member> getMembersFromKingdomByRank( Kingdom kingdom, KingdomRank rank) {

        ArrayList<Member> members = new ArrayList<Member>();

        for ( Member member : kingdom.getMembers() ) {

            if ( member.getRank().equals( rank ) ) {
                members.add(member);
            }

        }

        if ( members.size() > 0 ) {
            return members;
        }

        return null;
    }

    public ArrayList<String> getMembersNameFromKingdomByRank( Kingdom kingdom, KingdomRank rank) {

        ArrayList<String> members = new ArrayList<String>();

        for ( Member member : kingdom.getMembers() ) {

            if ( member.getRank().equals( rank ) ) {
                members.add(member.getName());
            }

        }

        if ( members.size() > 0 ) {
            return members;
        }

        return null;
    }

    public Kingdom getKingdomByInvitedPlayer( Player player ) {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                for ( Member member : kingdom.getInvitedMembers() ) {

                    if ( member.getUniqueId().equals( player.getUniqueId() ) ) {
                        return kingdom;
                    }

                }

            }

        }

        return null;
    }

    public Kingdom getKingdomByLocation(Location location) {

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                if ( kingdom.contains(location) ) {
                    return kingdom;
                }

            }

        }

        return null;
    }

    public ArrayList<Kingdom> getAllKingdomsNearLocation( Location location, Integer distance ) {

        ArrayList<Kingdom> nearKingdoms = new ArrayList<Kingdom>();

        if ( kingdoms.size() > 0 ) {

            for ( Kingdom kingdom : kingdoms ) {

                if ( kingdom.getBaseLocation().getBlockX() != 0 && kingdom.getBaseLocation().getBlockY() != 0 && kingdom.getBaseLocation().getBlockZ() != 0 ) {

                    if ( kingdom.getBaseLocation().getPosition().distance(location.getPosition()) < distance ) {
                        nearKingdoms.add(kingdom);
                    }

                }

            }

        }

        if ( nearKingdoms.size() > 0 ) {
            return nearKingdoms;
        }

        return null;
    }

}