package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import ch.nonameweb.kingdoms.kingdoms.task.zone.AttackTask;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.title.Title;

import java.util.function.Consumer;

public class AttackZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();

        Zone zone = getZoneManager().getZoneByName(name);

        if ( zone == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There is no Zone with this Name!"));
            return CommandResult.success();
        }

        if (!zone.isActive()) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "The Zone is not active!"));
            return CommandResult.success();
        }

        Kingdom attackKingdom = getKingdomManager().getKingdomByPlayer( srcPlayer );

        if ( attackKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if ( zone.getMinAttackers() > attackKingdom.getOnlineMembers().size() ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not enough Online Members!"));
            return CommandResult.success();
        }

        Kingdom defenseKingdom = getKingdomManager().getKingdomByName( zone.getKingdomName() );

        if ( defenseKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "No Kingdom owns this Zone! You can buy this Zone with /zone buy <name>"));
            return CommandResult.success();
        }

        if ( zone.getAttackCooldown() ) {
            // TODO Cooldown left
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't attack this Zone right now! You need to wait some Time."));
            return CommandResult.success();
        }

        if ( defenseKingdom.getOnlineMembers() == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There are no Defenders Online!"));
            return CommandResult.success();
        }

        if ( zone.getMinDefenders() > defenseKingdom.getOnlineMembers().size() ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There are not enough Defenders Online!"));
            return CommandResult.success();
        }

        attackKingdom.sendTitleToMembers(Title.of(Text.of(TextColors.GOLD, "In ", zone.getDelay(), "sek. könnt Ihr die Zone (", zone.getName(), ") von ", defenseKingdom.getName(), " angreifen!")));
        defenseKingdom.sendTitleToMembers(Title.of(Text.of(TextColors.RED, "Die Zone (", zone.getName(), ") wird in ", zone.getDelay()," sek. von ", attackKingdom.getName(), " angegriffen!")));

        zone.setAttack(true);

        // Start Attack Tasks
        Long delay = (long)zone.getDelay();
        Long repeating = (long)zone.getDuration();

        AttackTask task = new AttackTask(zone, attackKingdom, defenseKingdom);
        getTaskManager().createSyncRepeatingTask((Consumer<Task>)task, delay, repeating);
        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.attack")
                .arguments(
                        GenericArguments.string(Text.of("name"))
                )
                .executor(new AttackZoneCommand())
                .build();
    }

}