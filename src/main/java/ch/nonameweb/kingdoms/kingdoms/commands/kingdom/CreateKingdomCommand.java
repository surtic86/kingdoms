package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class CreateKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom != null ) {
            src.sendMessage(Text.of(TextColors.RED, "You are already in a Kingdom!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();
        String tag = args.<String>getOne(Text.of("tag")).get();


        if (name.isEmpty() || tag.isEmpty()) {
            src.sendMessage(Text.of(TextColors.RED, "/kingdom create <name> <tag>"));
            return CommandResult.success();
        }

        // Check Name
        Kingdom kingdom = getKingdomManager().getKingdomByName( name );

        if ( kingdom != null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is already a Kingdom with this Name!"));
            return CommandResult.success();
        }

        // Check Tag
        kingdom = getKingdomManager().getKingdomByTag(tag);

        if ( kingdom != null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is already a Kingdom with this Tag!"));
            return CommandResult.success();
        }

        srcMember.setRank( KingdomRank.LEADER );
        getKingdomManager().createKingdom(srcMember, name, tag);
        src.sendMessage(Text.of(TextColors.GOLD, "The Kingdom is created! Now you can invite Players with /kingdom invite <player>"));

        srcKingdom = getKingdomManager().getKingdomByPlayer(srcPlayer);
        srcKingdom.addToMessageChannels(srcMember, srcPlayer);

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.create")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.string(Text.of("tag")))
                .executor(new CreateKingdomCommand())
                .build();
    }

}