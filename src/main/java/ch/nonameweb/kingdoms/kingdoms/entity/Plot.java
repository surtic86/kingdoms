package ch.nonameweb.kingdoms.kingdoms.entity;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.Utils;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.Location;


@DatabaseTable(tableName = "plot")
public class Plot {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private int kingdom;

    @DatabaseField
    private int owner = 0;

    // Corner One
    @DatabaseField
    private Integer cornerOneX;

    @DatabaseField
    private Integer cornerOneY;

    @DatabaseField
    private Integer cornerOneZ;

    // Corner One
    @DatabaseField
    private Integer cornerTwoX;

    @DatabaseField
    private Integer cornerTwoY;

    @DatabaseField
    private Integer cornerTwoZ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKingdomName() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom.getName();
        }

        return null;
    }

    public Kingdom getKingdom() {

        Kingdom kingdom = Kingdoms.getInstance().getKingdomManager().getKingdomById(getKingdomId());

        if ( kingdom != null ) {
            return kingdom;
        }

        return null;
    }

    public int getKingdomId() {
        return kingdom;
    }

    public void setKingdom(int kingdom) {
        this.kingdom = kingdom;
    }

    public int getOwner() {
        return owner;
    }

    public String getOwnerName() {

        Member member = Kingdoms.getInstance().getMemberManager().getMemberById(owner);

        if ( member != null ) {
            return Utils.getUserName(member.getUniqueId());
        }

        return null;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public Integer getCornerOneX() {
        return cornerOneX;
    }

    public void setCornerOneX(Integer cornerOneX) {
        this.cornerOneX = cornerOneX;
    }

    public Integer getCornerOneY() {
        return cornerOneY;
    }

    public void setCornerOneY(Integer cornerOneY) {
        this.cornerOneY = cornerOneY;
    }

    public Integer getCornerOneZ() {
        return cornerOneZ;
    }

    public void setCornerOneZ(Integer cornerOneZ) {
        this.cornerOneZ = cornerOneZ;
    }

    public Location getCornerOne() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getCornerOneX(), getCornerOneY(), getCornerOneZ());
    }


    public Integer getCornerTwoX() {
        return cornerTwoX;
    }

    public void setCornerTwoX(Integer cornerTwoX) {
        this.cornerTwoX = cornerTwoX;
    }

    public Integer getCornerTwoY() {
        return cornerTwoY;
    }

    public void setCornerTwoY(Integer cornerTwoY) {
        this.cornerTwoY = cornerTwoY;
    }

    public Integer getCornerTwoZ() {
        return cornerTwoZ;
    }

    public void setCornerTwoZ(Integer cornerTwoZ) {
        this.cornerTwoZ = cornerTwoZ;
    }

    public Location getCornerTwo() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getCornerTwoX(), getCornerTwoY(), getCornerTwoZ());
    }

    public boolean contains(Location location) {

        if ( getCornerTwoX() >= location.getBlockX() && getCornerOneX() <= location.getBlockX()) {

            if ( getCornerTwoZ() >= location.getBlockZ() && getCornerOneZ() <= location.getBlockZ() ) {

                if ( getCornerTwoY() >= location.getBlockY() && getCornerOneY() <= location.getBlockY() ) {
                    return true;
                }

            }

        }

        return false;
    }

}
