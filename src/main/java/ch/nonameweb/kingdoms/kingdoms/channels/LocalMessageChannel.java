package ch.nonameweb.kingdoms.kingdoms.channels;

import ch.nonameweb.kingdoms.kingdoms.Config;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class LocalMessageChannel extends BaseMessageChannel {

    private Integer maxDistance;

    public LocalMessageChannel() {
        setPrefix(Text.of(TextColors.YELLOW, Config.confNode.getNode("Settings", "Chat", "Local", "Prefix").getString()));

        maxDistance = Config.confNode.getNode("Settings", "Chat", "Local", "MaxDistance").getInt();
    }

    @Override
    public Optional<Text> transformMessage(Object sender, MessageReceiver recipient, Text original, ChatType type) {

        if (sender instanceof Player && recipient instanceof Player) {

            Player senderPlayer = (Player) sender;
            Player recipientPlayer = (Player) recipient;

            if ( senderPlayer.getPosition().distance(recipientPlayer.getPosition()) < maxDistance ) {
                Text text = original;
                text = Text.of(prefix, " ", TextColors.RESET, text);

                return Optional.of(text);
            }

        }

       return Optional.empty();
    }

}
