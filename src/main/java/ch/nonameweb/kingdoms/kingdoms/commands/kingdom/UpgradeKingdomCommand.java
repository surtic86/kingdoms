package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.Config;
import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomLevel;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class UpgradeKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if (!srcMember.getRank().equals(KingdomRank.LEADER)) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Leader to do this!"));
            return CommandResult.success();
        }

        Integer maxLevel = Config.confNode.getNode("Settings", "Kingdoms", "MaxLevel").getInt();

        if ( srcKingdom.getLevel().getLevel() == maxLevel ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You have already the highest Level!"));
            return CommandResult.success();
        }

        KingdomLevel lvl = Config.getLevel(srcKingdom.getLevel().getLevel() + 1);

        if ( srcKingdom.getPoints() < lvl.getPoints() ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You have not enough Points!"));
            return CommandResult.success();
        }

        srcKingdom.removePoints( lvl.getPoints() );
        srcKingdom.setLevel( lvl.getLevel() );

        srcKingdom.sendMessageToMembers(Text.of(TextColors.GOLD, "The Kingdom has Upgraded to Level ", lvl.getLevel()));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.upgrade")
                .executor(new UpgradeKingdomCommand())
                .build();
    }

}