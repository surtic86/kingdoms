package ch.nonameweb.kingdoms.kingdoms.task.kingdom;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;

import java.util.function.Consumer;

public class SpawnTask implements Consumer<Task> {

    private Player player;
    private Location location;

    private int lastX;
    private int lastY;
    private int lastZ;

    public SpawnTask( Player player, Location location) {
        this.player = player;
        this.location = location;

        lastX = player.getLocation().getBlockX();
        lastY = player.getLocation().getBlockY();
        lastZ = player.getLocation().getBlockZ();
    }

    @Override
    public void accept(Task task) {

        if (
            player.getLocation().getBlockX() == lastX &&
            player.getLocation().getBlockY() == lastY &&
            player.getLocation().getBlockZ() == lastZ
        ) {
            player.setLocation(location);
        } else {
            player.sendMessage(Text.of(TextColors.RED, "Teleportation aborted! You have moved!"));
        }

        task.cancel();
    }
}
