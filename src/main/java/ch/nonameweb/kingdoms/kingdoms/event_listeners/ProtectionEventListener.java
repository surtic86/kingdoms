package ch.nonameweb.kingdoms.kingdoms.event_listeners;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.*;
import ch.nonameweb.kingdoms.kingdoms.manager.KingdomManager;
import ch.nonameweb.kingdoms.kingdoms.manager.MemberManager;
import ch.nonameweb.kingdoms.kingdoms.manager.PlotManager;
import ch.nonameweb.kingdoms.kingdoms.manager.ZoneManager;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Transform;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.entity.living.humanoid.player.RespawnPlayerEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;

public class ProtectionEventListener {

    private Kingdoms plugin;
    private MemberManager memberManager;
    private KingdomManager kingdomManager;
    private PlotManager plotManager;
    private ZoneManager zoneManager;

    public ProtectionEventListener() {
        plugin = Kingdoms.getInstance();
        memberManager = plugin.getMemberManager();
        kingdomManager = plugin.getKingdomManager();
        plotManager = plugin.getPlotManager();
        zoneManager = plugin.getZoneManager();
    }


    @Listener
    public void onMovePlayerEvent(MoveEntityEvent event, @Root Player player) {

        Location oldLoc = event.getFromTransform().getLocation();
        Location newLoc = event.getToTransform().getLocation();

        // nicht bewegt
        if(oldLoc.equals(newLoc)){
            return;
        }

        Kingdom oldLocKingdom = kingdomManager.getKingdomByLocation(oldLoc);
        Kingdom newLocKingdom = kingdomManager.getKingdomByLocation(newLoc);

        if (oldLocKingdom == null && newLocKingdom != null) {
            player.sendMessage(Text.of(TextColors.RED, "You entered Kingdom ", newLocKingdom.getName()));

        } else if (oldLocKingdom != null && newLocKingdom == null) {
            player.sendMessage(Text.of(TextColors.RED, "You leave Kingdom ", oldLocKingdom.getName()));

        } else if (oldLocKingdom != null && newLocKingdom != null) {

            Plot oldLocPlot = plotManager.getPlotByLocation(oldLoc);
            Plot newLocPlot = plotManager.getPlotByLocation(newLoc);

            if (oldLocPlot == null && newLocPlot != null) {
                player.sendMessage(Text.of(TextColors.GREEN, "You entered Plot ", newLocPlot.getId()));

            } else if (oldLocPlot != null && newLocPlot == null) {
                player.sendMessage(Text.of(TextColors.RED, "You leave Plot ", oldLocPlot.getId()));

            }

        }

        Zone oldLocZone = zoneManager.getZoneByLocation(oldLoc);
        Zone newLocZone = zoneManager.getZoneByLocation(newLoc);

        if (oldLocZone == null && newLocZone != null) {
            player.sendMessage(Text.of(TextColors.GREEN, "You entered Zone ", newLocZone.getName()));

        } else if (oldLocZone != null && newLocZone == null) {
            player.sendMessage(Text.of(TextColors.RED, "You leave Zone ", oldLocZone.getName()));

        }

    }

    @Listener
    public void onChangeBlockEventBreak(ChangeBlockEvent.Break event, @Root Player player) {
        onChangeBlockEvent(event, player);
    }

    @Listener
    public void onChangeBlockEventPlace(ChangeBlockEvent.Place event, @Root Player player) {
        onChangeBlockEvent(event, player);
    }


    private void onChangeBlockEvent(ChangeBlockEvent event, Player player) {

        Location blockLocation = event.getTransactions().get(0).getDefault().getLocation().orElseGet(null);

        if (blockLocation == null) {
            return;
        }

        Member member = memberManager.getMemberByPlayer(player);

        Kingdom locKingdom = kingdomManager.getKingdomByLocation(blockLocation);

        if (locKingdom != null) {

            Kingdom playerKingdom = kingdomManager.getKingdomByPlayer(player);

            if (locKingdom.getName().equals(playerKingdom.getName())) {


                Plot locPlot = plotManager.getPlotByLocation(blockLocation);

                if (locPlot != null) {


                    if (locPlot.getOwner() == member.getId()) {

                        // plot owner change block

                    } else if (
                        member.getRank().equals(KingdomRank.CAPTAIN) ||
                        member.getRank().equals(KingdomRank.LEADER)
                    ) {

                        // leader and captain can change block

                    } else {
                        event.setCancelled(true);
                        return;
                    }

                }

            } else {
                event.setCancelled(true);
                return;
            }

        }

        Zone zone = zoneManager.getZoneByLocation(blockLocation);

        if (zone != null) {

            Kingdom playerKingdom = kingdomManager.getKingdomByPlayer(player);

            if ( playerKingdom == null ) {

                player.sendMessage(Text.of(TextColors.RED, "Your not in a Kingdom to build in this Zone!"));
                event.setCancelled(true);
                return;

            } else if (zone.getKingdomId() == playerKingdom.getId()) {

                if (zone.isBuild()) {

                } else {
                    player.sendMessage(Text.of(TextColors.RED, "Building is not allowed in this Zone!"));
                    event.setCancelled(true);
                    return;
                }

            } else {
                player.sendMessage(Text.of(TextColors.RED, "Your Kingdom don't own this Zone!"));
                event.setCancelled(true);
                return;
            }

        }

    }

    @Listener
    public void onEntityDeath(DamageEntityEvent event) {

        Optional<EntityDamageSource> optDamageSource = event.getCause().first(EntityDamageSource.class);

        if (optDamageSource.isPresent()) {

            EntityDamageSource damageSource = optDamageSource.get();
            Entity killer = damageSource.getSource();

            if (killer instanceof Player) {

                if (event.getTargetEntity() instanceof Player) {

                    Location killedLocation = event.getTargetEntity().getLocation();

                    Kingdom kingdom = kingdomManager.getKingdomByLocation(killedLocation);

                    if (kingdom != null) {
                        event.setCancelled(true);
                    }

                    Zone zone = zoneManager.getZoneByLocation(killedLocation);

                    if (zone != null) {

                        if ( !zone.isAttack() ) {
                            event.setCancelled(true);
                            return;
                        }

                        Kingdom attackerKingdom = zone.getAttacker();
                        Kingdom defenderKingdom = zone.getDefender();

                        Member killedMember = memberManager.getMemberByPlayer((Player) event.getTargetEntity());
                        Member killerMember = memberManager.getMemberByPlayer((Player) killer);

                        if (

                            (
                                attackerKingdom.getName().equals(killedMember.getKingdomName()) &&
                                defenderKingdom.getName().equals(killerMember.getKingdomName())
                            ) || (

                                attackerKingdom.getName().equals(killerMember.getKingdomName()) &&
                                defenderKingdom.getName().equals(killedMember.getKingdomName())
                            )

                        ) {
                            // they can fight each other
                        } else {
                            event.setCancelled(true);
                        }

                    }

                }

            }

        }

    }


    @Listener
    public void obInteractBlock(InteractBlockEvent.Secondary event, @Root Player player) {

        Location blockLocation = event.getTargetBlock().getLocation().orElseGet(null);

        if (blockLocation == null) {
            return;
        }

        Member member = memberManager.getMemberByPlayer(player);

        Kingdom locKingdom = kingdomManager.getKingdomByLocation(blockLocation);

        if (locKingdom != null) {

            Kingdom playerKingdom = kingdomManager.getKingdomByPlayer(player);

            if (locKingdom.getName().equals(playerKingdom.getName())) {

                Plot locPlot = plotManager.getPlotByLocation(blockLocation);

                if (locPlot != null) {

                    if (locPlot.getOwner() == member.getId()) {

                        // plot owner is allowed

                    } else if (
                        member.getRank().equals(KingdomRank.CAPTAIN) ||
                        member.getRank().equals(KingdomRank.LEADER)
                    ) {

                        // leader and captain are allowed

                    } else {
                        player.sendMessage(Text.of(TextColors.RED, "Your not allowed to do this!"));
                        event.setCancelled(true);
                        return;
                    }

                }

            } else {
                event.setCancelled(true);
                return;
            }

        }

        Zone zone = zoneManager.getZoneByLocation(blockLocation);

        if (zone != null) {

            Kingdom playerKingdom = kingdomManager.getKingdomByPlayer(player);

            if ( playerKingdom == null ) {

                player.sendMessage(Text.of(TextColors.RED, "Your not allowed to do this!"));
                event.setCancelled(true);
                return;

            } else if (zone.getKingdomId() == playerKingdom.getId()) {

                // kingdom member is allowed

            } else {
                player.sendMessage(Text.of(TextColors.RED, "Your not allowed to do this!"));
                event.setCancelled(true);
                return;
            }

        }

    }


    @Listener
    public void onPlayerRespawn(RespawnPlayerEvent event, @First Player player) {

        Location fromLocation = event.getFromTransform().getLocation();

        Kingdom playerKingdom = kingdomManager.getKingdomByPlayer(player);

        Zone fromZone = zoneManager.getZoneByLocation(fromLocation);
        if (fromZone != null) {

            if (playerKingdom != null) {

                if ( fromZone.getKingdomId() == playerKingdom.getId() ) {

                    Transform<World> newTransform = new Transform<World>(fromZone.getSpawnDefenders());


                    if (fromZone.isAttack() && fromZone.getLifePoolDefenders() >= fromZone.getDeathCounterDefenders() ) {
                        fromZone.addDeathCounterDefenders();
                        event.setToTransform(newTransform);
                        return;

                    } else if (!fromZone.isAttack() ) {

                        event.setToTransform(newTransform);
                        return;

                    } else {
                        //
                    }

                } else if ( fromZone.getAttacker() != null && fromZone.getAttacker().getId() == playerKingdom.getId() ) {

                    if (fromZone.getLifePoolAttackers() >= fromZone.getDeathCounterAttacker()) {

                        Transform<World> newTransform = new Transform<World>(fromZone.getSpawnAttackers());
                        event.setToTransform(newTransform);

                        fromZone.addDeathCounterAttacker();

                        return;
                    }

                }

            }

        }


        if (playerKingdom != null && !event.isBedSpawn()) {

            if (playerKingdom.getSpawnX() != 0 || playerKingdom.getSpawnY() != 0 || playerKingdom.getSpawnZ() != 0) {

                Transform<World> newTransform = new Transform<World>(playerKingdom.getSpawnPoint());
                event.setToTransform(newTransform);

            }

        }

    }

}
