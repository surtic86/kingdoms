package ch.nonameweb.kingdoms.kingdoms.commands;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import ch.nonameweb.kingdoms.kingdoms.manager.*;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;

public class BaseCommand {

    protected Player srcPlayer = null;
    protected Member srcMember = null;
    protected Kingdom srcKingdom = null;

    protected Kingdoms getPluginInstance() {
        return Kingdoms.getInstance();
    }

    protected MemberManager getMemberManager() {
        return Kingdoms.getInstance().getMemberManager();
    }

    protected KingdomManager getKingdomManager() {
        return Kingdoms.getInstance().getKingdomManager();
    }

    protected ZoneManager getZoneManager() {
        return Kingdoms.getInstance().getZoneManager();
    }

    protected PlotManager getPlotManager() {
        return Kingdoms.getInstance().getPlotManager();
    }

    protected TaskManager getTaskManager() {
        return Kingdoms.getInstance().getTaskManager();
    }

    protected void initSrcPlayer(CommandSource src) {
        srcPlayer = (Player) src;
        srcMember = getMemberManager().getMemberByPlayer(srcPlayer);
        srcKingdom = getKingdomManager().getKingdomByPlayer(srcPlayer);
    }

}
