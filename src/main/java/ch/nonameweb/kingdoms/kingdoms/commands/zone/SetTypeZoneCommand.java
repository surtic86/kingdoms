package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import ch.nonameweb.kingdoms.kingdoms.entity.ZoneType;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SetTypeZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();
        String value= args.<String>getOne(Text.of("type")).get();

        Zone zone = getZoneManager().getZoneByName( name );

        if ( zone == null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is no Zone with this Name!"));
            return CommandResult.success();
        }


        if ( ZoneType.MILITARY.name().equalsIgnoreCase(value) ) {
            zone.setType(ZoneType.MILITARY);

        } else if ( ZoneType.MINE.name().equalsIgnoreCase(value) ) {
            zone.setType(ZoneType.MINE);

        }if ( ZoneType.CIVIL.name().equalsIgnoreCase(value) ) {
            zone.setType(ZoneType.CIVIL);

        } else {
            src.sendMessage(Text.of(TextColors.RED, "There is no Type with this Name!"));
            return CommandResult.success();
        }

        src.sendMessage(Text.of(TextColors.GOLD, "Type is set!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.settype")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.string(Text.of("type"))
                )
                .executor(new SetTypeZoneCommand())
                .build();
    }

}