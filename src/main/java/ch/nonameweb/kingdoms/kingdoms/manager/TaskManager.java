package ch.nonameweb.kingdoms.kingdoms.manager;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import org.spongepowered.api.scheduler.Task;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class TaskManager {

    private Kingdoms plugin;

    private ArrayList<Task> tasks;

    public TaskManager() {

        plugin = Kingdoms.getInstance();

        tasks = new ArrayList<Task>();
    }

    public void createAsyncDelayedTask(Consumer<Task> task, long delay) {

        Task stask = Task.builder().execute(task)
                .delay(delay, TimeUnit.SECONDS)
                .async()
                .submit(plugin);

        this.tasks.add(stask);
    }

    public void createSyncDelayedTask(Consumer<Task> task, long delay) {

        Task stask = Task.builder().execute(task)
                .delay(delay, TimeUnit.SECONDS)
                .submit(plugin);

        this.tasks.add(stask);
    }

    public void createSyncRepeatingTask(Consumer<Task> task, long delay, long repeating) {

        Task stask = Task.builder().execute(task)
                .delay(delay, TimeUnit.SECONDS)
                .interval(repeating, TimeUnit.SECONDS)
                .submit(plugin);

        this.tasks.add(stask);
    }

    public void stopAllTasks() {

        if (tasks.size() > 0) {

            for (Task task : tasks) {
                task.cancel();
            }

        }

    }

}