package ch.nonameweb.kingdoms.kingdoms;

import ch.nonameweb.kingdoms.kingdoms.channels.GlobalMessageChannel;
import ch.nonameweb.kingdoms.kingdoms.channels.LocalMessageChannel;
import ch.nonameweb.kingdoms.kingdoms.channels.StaffMessageChannel;
import ch.nonameweb.kingdoms.kingdoms.commands.ChannelCommand;
import ch.nonameweb.kingdoms.kingdoms.commands.kingdom.AcceptKingdomCommand;
import ch.nonameweb.kingdoms.kingdoms.commands.kingdom.BaseKingdomCommand;
import ch.nonameweb.kingdoms.kingdoms.commands.kingdom.RejectKingdomCommand;
import ch.nonameweb.kingdoms.kingdoms.commands.plot.BasePlotCommand;
import ch.nonameweb.kingdoms.kingdoms.commands.zone.BaseZoneCommand;
import ch.nonameweb.kingdoms.kingdoms.event_listeners.ProtectionEventListener;
import ch.nonameweb.kingdoms.kingdoms.event_listeners.SelectionEventListener;
import ch.nonameweb.kingdoms.kingdoms.event_listeners.StatsEventListener;
import ch.nonameweb.kingdoms.kingdoms.manager.*;
import ch.nonameweb.kingdoms.kingdoms.task.system.SaveTask;
import com.google.inject.Inject;
import com.j256.ormlite.db.MysqlDatabaseType;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppedServerEvent;
import org.spongepowered.api.plugin.Plugin;

import java.io.File;
import java.sql.SQLException;

@Plugin(
        id = "kingdoms",
        name = "Kingdoms",
        authors = {
                "Andreas Reinhold / surtic86"
        },
        description = "Kingdoms Plugin for MinePvP.de"
)
public class Kingdoms {

    public static Kingdoms instance;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private File file;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> loader;

    @Inject
    private Logger logger;

    private ConnectionSource connectionSource;

    private KingdomManager kingdomManager;
    private MemberManager memberManager;
    private ZoneManager zoneManager;
    private TaskManager taskManager;
    private PlotManager plotManager;


    private LocalMessageChannel localMessageChannel;
    private GlobalMessageChannel globalMessageChannel;
    private StaffMessageChannel staffMessageChannel;


    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("Kingdoms Plugin: loading..........");

        instance = this;

        Config.setup(file, loader);
        Config.load();
        logger.info("Kingdoms Plugin: Config loaded");

        this.createDBConnection();
        logger.info("Kingdoms Plugin: DB loaded");

        this.loadCommands();
        logger.info("Kingdoms Plugin: Commands loaded");

        this.loadMessageChannels();
        logger.info("Kingdoms Plugin: MessageChannels loaded");

        this.loadManagers();
        logger.info("Kingdoms Plugin: Manager loaded");

        this.loadEventListeners();
        logger.info("Kingdoms Plugin: EventListeners loaded");


        this.startSaveTask();
        logger.info("Kingdoms Plugin: SaveTask started");


        logger.info("Kingdoms Plugin: loaded!");
    }

    @Listener
    public void onServerStopped(GameStoppedServerEvent event) {
        logger.info("Kingdoms Plugin: saving..........");
        this.saveAll();
        logger.info("Kingdoms Plugin: saved!");
    }

    public void saveAll() {
        taskManager.stopAllTasks();
        memberManager.saveAll();
        kingdomManager.saveAll();
        zoneManager.saveAll();
        plotManager.saveAll();
    }

    private void loadManagers() {
        this.taskManager = new TaskManager();
        this.memberManager = new MemberManager();
        this.kingdomManager = new KingdomManager();
        this.zoneManager = new ZoneManager();
        this.plotManager = new PlotManager();
    }

    private void loadCommands() {
        Sponge.getCommandManager().register(instance, BaseKingdomCommand.build(), "kingdom", "king", "kg");
        Sponge.getCommandManager().register(instance, BaseZoneCommand.build(), "zone", "zn");
        Sponge.getCommandManager().register(instance, BasePlotCommand.build(), "plot");

        Sponge.getCommandManager().register(instance, AcceptKingdomCommand.build(), "accept");
        Sponge.getCommandManager().register(instance, RejectKingdomCommand.build(), "reject");

        Sponge.getCommandManager().register(instance, ChannelCommand.build(), "channel");
    }

    private void loadEventListeners() {
        Sponge.getEventManager().registerListeners(instance, new StatsEventListener());
        Sponge.getEventManager().registerListeners(instance, new SelectionEventListener());
        Sponge.getEventManager().registerListeners(instance, new ProtectionEventListener());
    }

    private void loadMessageChannels() {
        localMessageChannel = new LocalMessageChannel();
        globalMessageChannel = new GlobalMessageChannel();
        staffMessageChannel = new StaffMessageChannel();
    }


    private void startSaveTask() {
        SaveTask task = new SaveTask();
        Long time =  Config.confNode.getNode("Settings", "Task", "Save", "Time").getLong();
        taskManager.createSyncRepeatingTask( task, time, time);
    }


    public void createDBConnection() {

        String host = Config.confNode.getNode("Settings", "MySQL", "Host").getString();
        Integer port = Config.confNode.getNode("Settings", "MySQL", "Port").getInt();
        String database = Config.confNode.getNode("Settings", "MySQL", "DB").getString();
        String user = Config.confNode.getNode("Settings", "MySQL", "User").getString();
        String password = Config.confNode.getNode("Settings", "MySQL", "Password").getString();

        String url = "jdbc:mysql://"+host+":"+port+"/"+database;

        // pooled connection source
        try {
            connectionSource = new JdbcConnectionSource(url, user, password, new MysqlDatabaseType());
            ((JdbcConnectionSource) connectionSource).initialize();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ConnectionSource getConnectionSource() {
        return connectionSource;
    }

    public static Kingdoms getInstance() {
        return instance;
    }

    public MemberManager getMemberManager() {
        return memberManager;
    }

    public KingdomManager getKingdomManager() {
        return kingdomManager;
    }

    public ZoneManager getZoneManager() {
        return zoneManager;
    }

    public TaskManager getTaskManager() {
        return taskManager;
    }

    public PlotManager getPlotManager() {
        return plotManager;
    }

    public Logger getLogger() {
        return logger;
    }

    public LocalMessageChannel getLocalMessageChannel() {
        return localMessageChannel;
    }

    public GlobalMessageChannel getGlobalChannel() {
        return globalMessageChannel;
    }

    public StaffMessageChannel getStaffMessageChannel() {
        return staffMessageChannel;
    }
}
