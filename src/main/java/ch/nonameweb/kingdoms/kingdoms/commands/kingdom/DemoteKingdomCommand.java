package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class DemoteKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {

            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!"));
            return CommandResult.success();
        }

        Player demotePlayer = args.<Player>getOne("demotePlayer").get();
        Member demoteMember = getMemberManager().getMemberByPlayer(demotePlayer);

        if ( demoteMember == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There is no Player with this Name!"));
            return CommandResult.success();
        }

        if ( !srcMember.getKingdomName().equalsIgnoreCase( demoteMember.getKingdomName() ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "The Player is not in your Kingdom!"));
            return CommandResult.success();
        }

        if ( demoteMember.getRank().equals( KingdomRank.LEADER ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't demote a Leader!"));
            return CommandResult.success();
        }

        if ( srcMember.getRank().equals( KingdomRank.CAPTAIN ) ) {

            if ( demoteMember.getRank().equals( KingdomRank.CAPTAIN ) ) {
                srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't demote a Member who has the same Rank!"));
                return CommandResult.success();
            }

        }

        if ( demoteMember.getRank().equals( KingdomRank.NOVICE ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't demote a Novice!"));
            return CommandResult.success();
        }

        if ( demoteMember.getRank().equals( KingdomRank.MEMBER ) ) {
            demoteMember.setRank( KingdomRank.NOVICE );
        } else if ( demoteMember.getRank().equals( KingdomRank.CAPTAIN ) ) {
            demoteMember.setRank( KingdomRank.MEMBER );
        }

        srcKingdom.sendMessageToMembers(Text.of(TextColors.GOLD ,demotePlayer.getName(), " was demoted to ", demoteMember.getRank()));

        Optional<Player> promotePlayer = Sponge.getServer().getPlayer(demotePlayer.getName());
        if (promotePlayer.isPresent()) {
            promotePlayer.get().sendMessage( Text.of(TextColors.GOLD , "You where demoted to ", demoteMember.getRank()) );
        }

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.demote")
                .arguments(
                        GenericArguments.player(Text.of("demotePlayer"))
                )
                .executor(new DemoteKingdomCommand())
                .build();
    }
}
