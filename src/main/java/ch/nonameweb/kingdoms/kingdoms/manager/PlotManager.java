package ch.nonameweb.kingdoms.kingdoms.manager;


import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import ch.nonameweb.kingdoms.kingdoms.entity.Plot;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.spongepowered.api.world.Location;

import java.sql.SQLException;
import java.util.ArrayList;

public class PlotManager {

    private Kingdoms plugin;

    private ConnectionSource connectionSource;
    private Dao<Plot, String> dao;

    private ArrayList<Plot> plots;

    public PlotManager() {
        plugin = Kingdoms.getInstance();
        connectionSource = plugin.getConnectionSource();

        try {
            dao = DaoManager.createDao(connectionSource, Plot.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        plots = new ArrayList<Plot>();

        load();
    }

    /**
     * Load all Member Entries
     */
    private void load() {

        try {
            TableUtils.createTableIfNotExists(connectionSource, Plot.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            plots = (ArrayList) dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        plugin.getLogger().info("loaded Plots : " + plots.size());

    }

    public void saveAll() {

        if ( plots.size() > 0 ) {

            for ( Plot plot : plots ) {
                try {
                    dao.createOrUpdate(plot);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public void createPlot( Kingdom kingdom, String name, Location cornerOne, Location cornerTwo) {

        Plot plot = new Plot();
        plot.setKingdom( kingdom.getId() );
        plot.setName(name);

        // Corner One
        plot.setCornerOneX( cornerOne.getBlockX() );
        plot.setCornerOneY( cornerOne.getBlockY() );
        plot.setCornerOneZ( cornerOne.getBlockZ() );

        // Corner Two
        plot.setCornerTwoX( cornerTwo.getBlockX() );
        plot.setCornerTwoY( cornerTwo.getBlockY() );
        plot.setCornerTwoZ( cornerTwo.getBlockZ() );

        try {
            dao.createOrUpdate(plot);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        plots.add(plot);
    }

    public void deletePlot( Plot plot ) {
        plots.remove(plot);
        try {
            dao.delete(plot);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Plot> getPlotsByKingdom( Kingdom kingdom ) {

        ArrayList<Plot> kingdomPlots = new ArrayList<Plot>();

        if ( this.plots.size() > 0 ) {

            for ( Plot plot : this.plots ) {

                if ( plot.getKingdomName().equalsIgnoreCase( kingdom.getName() ) ) {
                    kingdomPlots.add(plot);
                }

            }

        }

        return kingdomPlots;
    }

    public Plot getPlotFromKingdomByLocation(Kingdom kingdom, Location location ) {

        ArrayList<Plot> kingdomPlots = getPlotsByKingdom(kingdom);

        if ( kingdomPlots != null ) {

            for ( Plot plot : kingdomPlots ) {

                if ( plot.contains( location ) ) {
                    return plot;
                }

            }

        }

        return null;
    }

    public Plot getPlotByLocation(Location location) {

        if ( plots.size() > 0 ) {

            for ( Plot plot : plots ) {

                if ( plot.contains( location ) ) {
                    return plot;
                }

            }

        }

        return null;
    }

    public Plot getPlotById( Integer id ) {

        if ( plots.size() > 0 ) {

            for ( Plot plot : plots ) {

                if ( plot.getId() == id ) {
                    return plot;
                }

            }

        }

        return null;
    }

    public Plot getPlotByNameAndKingdom( String name, Kingdom kingdom ) {

        if ( plots.size() > 0 ) {

            for ( Plot plot : plots ) {

                if ( plot.getName().equalsIgnoreCase(name) && plot.getKingdomId() == kingdom.getId() ) {
                    return plot;
                }

            }

        }

        return null;
    }

    public ArrayList<Plot> getPlotsByMember( Member member ) {

        ArrayList<Plot> plots = new ArrayList<Plot>();

        if ( plots.size() > 0 ) {

            for ( Plot plot : this.plots ) {

                if ( plot.getOwner() == member.getId() ) {
                    plots.add(plot);
                }

            }

            if ( plots.size() > 0 ) {
                return plots;
            }

        }

        return null;
    }

    public void removeOwnerFromPlots( Member member ) {

        if ( plots.size() > 0 ) {

            for ( Plot plot : this.plots ) {

                if ( plot.getOwner() == member.getId() ) {
                    plot.setOwner(0);
                }

            }

        }

    }

}

