package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.Config;
import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.task.kingdom.SpawnTask;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SpawnKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }


        if ( srcKingdom.getSpawnX() == 0 && srcKingdom.getSpawnY() == 0 && srcKingdom.getSpawnZ() == 0 ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "Your Kingdom have no Spawn Point!"));
            return CommandResult.success();
        }


        Integer teleport_delay = Config.confNode.getNode("Settings", "Teleport", "Delay").getInt();

        getTaskManager().createSyncDelayedTask(new SpawnTask(srcPlayer, srcKingdom.getSpawnPoint()), teleport_delay);

        srcPlayer.sendMessage(Text.of(TextColors.RED, "You will be Teleportet in ", teleport_delay, " sec! Don't move!"));

        return CommandResult.success();
    }
    
    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.setspwan")
                .executor(new SpawnKingdomCommand())
                .build();
    }

}