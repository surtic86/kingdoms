package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

public class InviteKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!") );
            return CommandResult.success();
        }

        Optional<Player> invitedPlayer = args.getOne(Text.of("invitedPlayer"));

        if (!invitedPlayer.isPresent()) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There is no Player with this Name!"));
            return CommandResult.success();
        }

        Player invitedPlayerObj = invitedPlayer.get();

        Member invitedMember = getMemberManager().getMemberByPlayer(invitedPlayerObj);

        srcKingdom.addInvitedMember(invitedMember);

        srcPlayer.sendMessage(Text.of(TextColors.GOLD, "Player is invited!"));
        invitedPlayerObj.sendMessage(Text.of(TextColors.GOLD, "You are invited from ", TextColors.YELLOW ,srcPlayer.getName(), TextColors.GOLD," to join the Kingdom ", TextColors.YELLOW, srcKingdom.getName()));
        invitedPlayerObj.sendMessage(Text.of(TextColors.GOLD, "Type ", TextColors.GREEN, "/accept ", TextColors.GOLD, "ore ", TextColors.RED, "/reject"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.invite")
                .arguments(
                        GenericArguments.player(Text.of("invitedPlayer"))
                )
                .executor(new InviteKingdomCommand())
                .build();
    }

}