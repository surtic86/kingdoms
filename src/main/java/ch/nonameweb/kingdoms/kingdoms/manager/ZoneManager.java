package ch.nonameweb.kingdoms.kingdoms.manager;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import ch.nonameweb.kingdoms.kingdoms.task.zone.PointsTask;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class ZoneManager {

    private Kingdoms plugin;

    private ConnectionSource connectionSource;
    private Dao<Zone, String> dao;

    private KingdomManager kingdomManager;

    private ArrayList<Zone> zones;


    public ZoneManager() {
        plugin = Kingdoms.getInstance();
        connectionSource = plugin.getConnectionSource();


        try {
            dao = DaoManager.createDao(connectionSource, Zone.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        kingdomManager = plugin.getKingdomManager();

        zones = new ArrayList<Zone>();

        load();

        plugin.getTaskManager().createSyncRepeatingTask( new PointsTask(), 2160000L, 2160000L);
    }

    public void load() {

        try {
            TableUtils.createTableIfNotExists(connectionSource, Zone.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            zones = (ArrayList) dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        plugin.getLogger().info("loaded Zones : " + zones.size());
    }

    public void saveAll() {

        if ( zones.size() > 0 ) {

            for ( Zone zone : zones ) {

                try {
                    dao.createOrUpdate(zone);
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    public ArrayList<Zone> getAllZones() {
        return zones;
    }

    public void createZone(String name, Location cornerOne, Location cornerTwo ) {

        Zone zone = new Zone();
        zone.setName(name);

        // Corner One
        zone.setCornerOneX( cornerOne.getBlockX() );
        zone.setCornerOneY( cornerOne.getBlockY() );
        zone.setCornerOneZ( cornerOne.getBlockZ() );

        // Corner Two
        zone.setCornerTwoX( cornerTwo.getBlockX() );
        zone.setCornerTwoY( cornerTwo.getBlockY() );
        zone.setCornerTwoZ( cornerTwo.getBlockZ() );

        try {
            dao.create(zone);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        zones.add(zone);
    }

    public void deleteZone( Zone zone ) {
        zones.remove(zone);

        try {
            dao.delete(zone);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Zone getZoneByName( String name ) {

        if ( zones.size() > 0 ) {

            for ( Zone zone : zones ) {

                if ( zone.getName().equalsIgnoreCase( name ) ) {
                    return zone;
                }

            }

        }

        return null;
    }

    public Zone getZoneByLocation(Location location ) {

        if ( zones.size() > 0 ) {

            for ( Zone zone : zones ) {

                if ( zone.contains(location) ) {
                    return zone;
                }

            }

        }

        return null;
    }

    public ArrayList<Zone> getAllZonesByKingdom( Kingdom kingdom ) {

        ArrayList<Zone> zones = new ArrayList<Zone>();

        if ( this.zones != null ) {

            for ( Zone zone : this.zones ) {

                if ( zone.getKingdom().equals( kingdom ) ) {
                    zones.add(zone);
                }

            }

        }

        if ( zones.size() > 0 ) {
            return zones;
        }

        return null;
    }

    public Zone getAttackedZoneByKingdom( Kingdom kingdom ) {

        if ( this.zones != null ) {

            for ( Zone zone : this.zones ) {

                if ( zone.getAttacker().equals( kingdom ) ) {
                    return zone;
                }

            }

        }

        return null;
    }

    public ArrayList<Player> getOnlinePlayersInZone(Zone zone ) {

        ArrayList<Player> players = new ArrayList<Player>();

        Collection<Player> playerCollection = Sponge.getServer().getOnlinePlayers();


        if ( !playerCollection.isEmpty() ) {

            for ( Player player : playerCollection ) {

                if ( zone.contains( player.getLocation() ) ) {
                    players.add(player);
                }

            }

        }

        if ( players.size() > 0 ) {
            return players;
        }

        return null;
    }

    public ArrayList<Player> getOnlinePlayersFromKingdomNearZoneFlag( Zone zone, Kingdom kingdom ) {

        ArrayList<Player> players = new ArrayList<Player>();

        Collection<Player> playerCollection = Sponge.getServer().getOnlinePlayers();

        if ( !playerCollection.isEmpty() ) {

            for ( Player player : Sponge.getServer().getOnlinePlayers() ) {

                Kingdom playerKingdom = kingdomManager.getKingdomByPlayer(player);

                if ( playerKingdom != null ) {

                    if ( playerKingdom.equals( kingdom ) ) {

                        if ( zone.isNearFlag( player.getLocation() ) ) {
                            players.add(player);
                        }

                    }

                }

            }

        }

        if ( players.size() > 0 ) {
            return players;
        }

        return null;
    }

    public void resetAllZones() {

        if ( zones.size() > 0 ) {

            for ( Zone zone : zones ) {
                resetZone(zone);
            }

        }

    }

    public void resetZone( Zone zone ) {

        World world = Sponge.getServer().getWorld("world").get();

        resetFlag(world, zone);

        zone.setAttacker(null);
        zone.setDefender(null);

        zone.setDeathCounterAttacker(0);
        zone.setDeathCounterDefenders(0);

        zone.setAttack(false);
    }

   public void createFlag(World world, Zone zone ) {

        // Ground
        createGround(world, zone.getFlagX() - 4, zone.getFlagX() + 4, zone.getFlagY(), zone.getFlagY(), zone.getFlagZ() - 4, zone.getFlagZ() + 4, BlockTypes.STONE.getDefaultState());
        createGround(world, zone.getFlagX() - 2, zone.getFlagX() + 2, zone.getFlagY() + 1, zone.getFlagY() + 1, zone.getFlagZ() - 2, zone.getFlagZ() + 2, BlockTypes.STONE_SLAB.getDefaultState());
        createGround(world, zone.getFlagX() - 1, zone.getFlagX() + 1, zone.getFlagY() + 1, zone.getFlagY() + 1, zone.getFlagZ() - 1, zone.getFlagZ() + 1, BlockTypes.STONE.getDefaultState());
        
        world.setBlock( zone.getFlagX(), zone.getFlagY() + 1, zone.getFlagZ(), BlockTypes.GLOWSTONE.getDefaultState());

        // Pole
        for ( int i = 1; i <= 12; i++ ) {
            world.setBlock( zone.getFlagX() -1, zone.getFlagY() + 1 + i, zone.getFlagZ(), BlockTypes.DARK_OAK_FENCE.getDefaultState());
        }

        // Flag
        world.setBlock( zone.getFlagX(), zone.getFlagY() +13, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX(), zone.getFlagY() +12, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +13, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +12, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());

    }

    public void removeFlag( World world, Zone zone ) {

        // Ground
        createGround(world, zone.getFlagX() - 4, zone.getFlagX() + 4, zone.getFlagY(), zone.getFlagY(), zone.getFlagZ() - 4, zone.getFlagZ() + 4, BlockTypes.AIR.getDefaultState());
        createGround(world, zone.getFlagX() - 2, zone.getFlagX() + 2, zone.getFlagY() + 1, zone.getFlagY() + 1, zone.getFlagZ() - 2, zone.getFlagZ() + 2, BlockTypes.AIR.getDefaultState());
        createGround(world, zone.getFlagX() - 1, zone.getFlagX() + 1, zone.getFlagY() + 1, zone.getFlagY() + 1, zone.getFlagZ() - 1, zone.getFlagZ() + 1, BlockTypes.AIR.getDefaultState());

        // Pole
        for ( int i = 1; i <= 12; i++ ) {
            world.setBlock( zone.getFlagX() -1, zone.getFlagY() + 1 + i, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        }

        // Flag
        world.setBlock( zone.getFlagX(), zone.getFlagY() +13, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        world.setBlock( zone.getFlagX(), zone.getFlagY() +12, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +13, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +12, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());

    }

    public void updateFlag( World world, Zone zone, Integer old, Integer difference ) {

        world.setBlock( zone.getFlagX(), zone.getFlagY() + 3 + old, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        world.setBlock( zone.getFlagX(), zone.getFlagY() + 2 + old, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() + 3 + old, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() + 2 + old, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());

        world.setBlock( zone.getFlagX(), zone.getFlagY() + 3 + old + difference, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX(), zone.getFlagY() + 2 + old + difference, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() + 3 + old + difference, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() + 2 + old + difference, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());

    }

    public void resetFlag( World world, Zone zone ) {

        for ( int i = 2; i <= 12; i ++ ) {

            world.setBlock( zone.getFlagX(), zone.getFlagY() +i, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());
            world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +i, zone.getFlagZ(), BlockTypes.AIR.getDefaultState());

        }

        // Flag
        world.setBlock( zone.getFlagX(), zone.getFlagY() +13, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX(), zone.getFlagY() +12, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +13, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());
        world.setBlock( zone.getFlagX() + 1, zone.getFlagY() +12, zone.getFlagZ(), BlockTypes.WOOL.getDefaultState());

    }


    private void createGround(World world, int xMin, int xMax, int yMin, int yMax, int zMin, int zMax, BlockState blockState) {

        for ( int x = xMin; x <=  xMax; x++ ) {

            for ( int z = zMin; z <=  zMax; z++ ) {

                for ( int y = yMin; y <=  yMax; y++ ) {
                    world.setBlock(x,y,z, blockState);
                }

            }

        }

    }

}
