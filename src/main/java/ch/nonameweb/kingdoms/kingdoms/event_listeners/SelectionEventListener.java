package ch.nonameweb.kingdoms.kingdoms.event_listeners;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import ch.nonameweb.kingdoms.kingdoms.manager.KingdomManager;
import ch.nonameweb.kingdoms.kingdoms.manager.MemberManager;
import ch.nonameweb.kingdoms.kingdoms.manager.PlotManager;
import ch.nonameweb.kingdoms.kingdoms.manager.ZoneManager;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;

public class SelectionEventListener {

    private Kingdoms plugin;
    private MemberManager memberManager;
    private KingdomManager kingdomManager;
    private PlotManager plotManager;
    private ZoneManager zoneManager;

    public SelectionEventListener() {
        plugin = Kingdoms.getInstance();
        memberManager = plugin.getMemberManager();
        kingdomManager = plugin.getKingdomManager();
        plotManager = plugin.getPlotManager();
        zoneManager = plugin.getZoneManager();
    }

    @Listener
    public void onInteractBlockEventPrimaryMainHand(InteractBlockEvent.Primary.MainHand event, @Root Player player) {
        Member member = memberManager.getMemberByPlayer(player);
        member.getSelection().handleInteractBlockEvent(event, player, true);
    }

    @Listener
    public void onInteractBlockEventSecondaryMainHand(InteractBlockEvent.Secondary.MainHand event, @Root Player player) {
        Member member = memberManager.getMemberByPlayer(player);
        member.getSelection().handleInteractBlockEvent(event, player, false);
    }

}
