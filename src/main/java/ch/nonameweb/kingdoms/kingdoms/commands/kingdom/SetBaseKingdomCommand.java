package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.Config;
import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SetBaseKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (!srcMember.getRank().equals(KingdomRank.LEADER)) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Leader to do this!"));
            return CommandResult.success();
        }

        Integer base_min_players = Config.confNode.getNode("Settings", "Kingdoms", "Base", "MinPlayers").getInt();
        Integer distance_min_spawn = Config.confNode.getNode("Settings", "Kingdoms", "Distance", "MinSpawn").getInt();
        Integer distance_min_kingdom = Config.confNode.getNode("Settings", "Kingdoms", "Distance", "MinKingdoms").getInt();
        Integer distance_min_zone = Config.confNode.getNode("Settings", "Kingdoms", "Distance", "MinZones").getInt();

        if ( srcKingdom.getMembers().size() < base_min_players ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You have not enough Members!"));
            return CommandResult.success();
        }

        if ( srcKingdom.getBaseX() != 0 && srcKingdom.getBaseY() != 0 && srcKingdom.getBaseZ() != 0 ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You have already a Base!"));
            return CommandResult.success();
        }

        if ( !srcPlayer.getWorld().getName().equalsIgnoreCase("world") ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in the Main World!"));
            return CommandResult.success();
        }


        Double distance_spwan= srcPlayer.getPosition().distance( srcPlayer.getWorld().getSpawnLocation().getPosition() );

        getPluginInstance().getLogger().info("Kingdoms: Command SetBase: distance_spawn " + distance_spwan + " distance_min_spawn " + distance_min_spawn);

        // Check Ranges
        if ( distance_spwan < distance_min_spawn ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are to near to the Spawn! ", distance_spwan, " of ", distance_min_spawn));
            return CommandResult.success();
        }


        Double distance_kingdom = 0.0;

        if ( getKingdomManager().getAllKingdoms().size() > 0 ) {

            for ( Kingdom nearKingdom : getKingdomManager().getAllKingdoms() ) {

                if ( nearKingdom.getBaseX() != 0 && nearKingdom.getBaseY() != 0 && nearKingdom.getBaseZ() != 0 ) {

                    distance_kingdom = srcPlayer.getPosition().distance( nearKingdom.getBasePosition() );

                    if ( distance_kingdom < distance_min_kingdom ) {
                        srcPlayer.sendMessage(Text.of(TextColors.RED, "You are to near to the Kingdom ", nearKingdom.getName(), "! ", distance_kingdom, " of ", distance_min_spawn));
                        return CommandResult.success();
                    }

                }

            }

        }

        Double distance_zone = 0.0;

        if ( getZoneManager().getAllZones().size() > 0 ) {

            for ( Zone nearZone : getZoneManager().getAllZones() ) {

                if ( nearZone.getFlagX() != 0 && nearZone.getFlagX() != 0 && nearZone.getFlagZ() != 0 ) {

                    distance_zone = srcPlayer.getPosition().distance( nearZone.getFlag().getPosition() );

                    if ( distance_zone < distance_min_zone ) {
                        srcPlayer.sendMessage(Text.of(TextColors.RED, "You are to near to the Zone ", nearZone.getName(), "! ", distance_zone, " of ", distance_min_zone));
                        return CommandResult.success();
                    }

                }

            }

        }

        srcKingdom.setBaseX( srcPlayer.getLocation().getBlockX() );
        srcKingdom.setBaseY( srcPlayer.getLocation().getBlockY() );
        srcKingdom.setBaseZ( srcPlayer.getLocation().getBlockZ() );


        srcPlayer.sendMessage(Text.of(TextColors.GOLD, "Base set!"));

        return CommandResult.success();
    }

    
    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.setbase")
                .executor(new SetBaseKingdomCommand())
                .build();
    }

}