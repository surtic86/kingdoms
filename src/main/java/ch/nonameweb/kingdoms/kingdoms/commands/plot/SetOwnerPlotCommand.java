package ch.nonameweb.kingdoms.kingdoms.commands.plot;

import ch.nonameweb.kingdoms.kingdoms.Utils;
import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import ch.nonameweb.kingdoms.kingdoms.entity.Plot;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SetOwnerPlotCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!") );
            return CommandResult.success();
        }

        String plotName = args.<String>getOne(Text.of("name")).get();
        Player ownerPlayer = args.<Player>getOne(Text.of("owner")).orElseGet(null);

        Plot plot = getPlotManager().getPlotByNameAndKingdom( plotName, srcKingdom );

        if ( plot == null ) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "There is no Plot with this Name!") );
            srcPlayer.sendMessage( Text.of(TextColors.RED, "/plot setowner <plotid> <player>") );
            return CommandResult.success();
        }

        Member ownerMember = getMemberManager().getMemberByPlayer( ownerPlayer );

        if ( ownerMember == null ) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "There is no Player with this Name!") );
            srcPlayer.sendMessage( Text.of(TextColors.RED, "/plot setowner <name> <player>") );
            return CommandResult.success();
        }

        if (!srcMember.getKingdomName().equalsIgnoreCase(ownerMember.getKingdomName())) {
            srcPlayer.sendMessage( Text.of(TextColors.RED, "The Player is not in your Kingdom!") );
            return CommandResult.success();
        }

        plot.setOwner( ownerMember.getId() );

        if ( ownerPlayer.isOnline() ) {
            ownerPlayer.sendMessage( Text.of(TextColors.GOLD, "You are now Owner of the Plot ", Utils.getUserName(ownerMember.getUniqueId()) ) );
        }

        srcPlayer.sendMessage( Text.of(TextColors.GOLD, "Owner set") );

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.invite")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.player(Text.of("owner"))
                )
                .executor(new SetOwnerPlotCommand())
                .build();
    }

}