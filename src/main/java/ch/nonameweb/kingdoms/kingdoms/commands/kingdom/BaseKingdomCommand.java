package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class BaseKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Help"));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        if ( srcPlayer.hasPermission("kingdoms.command.kingdom.list") ) {
            srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom list -> shows all Kingdoms"));
        }

        if ( srcPlayer.hasPermission("kingdoms.command.kingdom.info") ) {
            srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom info <name|tag|empty>-> shows Info about a Kingdom"));
        }

        if (srcKingdom != null) {

            if ( srcPlayer.hasPermission("kingdoms.command.kingdom.members") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom members -> list Kingdom Members"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.kingdom.spawn") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom spawn -> Spwan at Kingdom"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.kingdom.leave") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom leave -> leave Kingdom"));
            }

            if (
                srcMember.getRank().equals(KingdomRank.LEADER) ||
                srcMember.getRank().equals(KingdomRank.CAPTAIN)
            ) {

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.invite") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom invite <player> -> invite Player"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.kick") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom kick <player> -> kick Player"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.promote") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom promote <player> -> promote Player"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.demote") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom demote <player> -> demote Player"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.upgrade") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom upgrade -> upgrade Kingdom"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.setbase") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom setbase -> set Base"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.kingdom.setspawn") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom setspwan -> set Spwan"));
                }

            }

        } else {

            if ( srcPlayer.hasPermission("kingdoms.command.kingdom.create") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom create <name> <tag> -> create a Kingdom"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.kingdom.accept") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom accept -> accept Kingdom invite"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.kingdom.reject") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "kingdom reject -> reject Kingdom invite"));
            }

        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom")
                .child(CreateKingdomCommand.build(), "create")
                .child(ListKingdomCommand.build(), "list")
                .child(LeaveKingdomCommand.build(), "leave")
                .child(InfoKingdomCommand.build(), "info")
                .child(MembersKingdomCommand.build(), "members")
                .child(InviteKingdomCommand.build(), "invite")
                .child(AcceptKingdomCommand.build(), "accept")
                .child(RejectKingdomCommand.build(), "reject")
                .child(KickKingdomCommand.build(), "kick")
                .child(PromoteKingdomCommand.build(), "promote")
                .child(DemoteKingdomCommand.build(), "demote")
                .child(UpgradeKingdomCommand.build(), "upgrade")
                .child(SetBaseKingdomCommand.build(), "setbase")
                .child(SetSpawnKingdomCommand.build(), "setspawn")
                .child(SpawnKingdomCommand.build(), "spawn")
                .executor(new BaseKingdomCommand())
                .build();
    }

}
