package ch.nonameweb.kingdoms.kingdoms.commands;

import ch.nonameweb.kingdoms.kingdoms.entity.KingdomChannel;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ChannelCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        // Switch the ChatChannel to Chat
        if ( srcMember.getChannel().equals( KingdomChannel.LOCAL ) ) {

            if ( srcKingdom != null ) {

                srcMember.setChannel( KingdomChannel.KINGDOM );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Kingdom Chat!"));

            } else {

                if ( srcPlayer.hasPermission("kingdoms.chat.global") ) {
                    srcMember.setChannel( KingdomChannel.GLOBAL );
                    srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Global Chat!"));
                }

            }

        } else if ( srcMember.getChannel().equals( KingdomChannel.KINGDOM ) ) {

            if ( srcMember.getRank() == KingdomRank.CAPTAIN || srcMember.getRank() == KingdomRank.LEADER ) {
                srcMember.setChannel( KingdomChannel.KINGDOM_STAFF );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Kingdom Staff Chat!"));
            } else {
                srcMember.setChannel( KingdomChannel.LOCAL );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Local Chat!"));
            }

        } else if ( srcMember.getChannel().equals( KingdomChannel.KINGDOM_STAFF ) ) {

            if ( srcPlayer.hasPermission("kingdoms.chat.global") ) {
                srcMember.setChannel( KingdomChannel.GLOBAL );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Global Chat!"));
            } else {
                srcMember.setChannel( KingdomChannel.LOCAL );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Local Chat!"));
            }

        } else if ( srcMember.getChannel().equals( KingdomChannel.GLOBAL ) ) {

            if ( srcPlayer.hasPermission("kingdoms.chat.staff") ) {
                srcMember.setChannel( KingdomChannel.STAFF );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Staff Chat!"));
            } else {
                srcMember.setChannel( KingdomChannel.LOCAL );
                srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Local Chat!"));

            }

        } else if ( srcMember.getChannel().equals( KingdomChannel.STAFF ) ) {

            srcMember.setChannel( KingdomChannel.LOCAL );
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "You are now in Local Chat!"));

        }

        srcMember.updateMessageChannel();

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.channel")
                .executor(new ChannelCommand())
                .build();
    }

}