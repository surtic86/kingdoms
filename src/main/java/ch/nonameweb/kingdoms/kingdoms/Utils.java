package ch.nonameweb.kingdoms.kingdoms;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.service.user.UserStorageService;

import java.util.Optional;
import java.util.UUID;

public class Utils {

    public static Optional<User> getUser(UUID uuid) {
        Optional<UserStorageService> userStorage = Sponge.getServiceManager().provide(UserStorageService.class);
        return userStorage.get().get(uuid);
    }

    public static String getUserName(UUID uuid) {

        User user = Utils.getUser(uuid).orElseGet(null);

        if (user == null) {
            return "UNKNOWN";
        }

        return user.getName();
    }

}
