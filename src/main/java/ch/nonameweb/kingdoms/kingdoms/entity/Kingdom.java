package ch.nonameweb.kingdoms.kingdoms.entity;

import ch.nonameweb.kingdoms.kingdoms.Config;
import ch.nonameweb.kingdoms.kingdoms.channels.KingdomMessageChannel;
import ch.nonameweb.kingdoms.kingdoms.channels.KingdomStaffMessageChannel;
import com.flowpowered.math.vector.Vector3d;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.title.Title;
import org.spongepowered.api.world.Location;

import java.util.ArrayList;

@DatabaseTable(tableName = "kingdom")
public class Kingdom {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String tag;

    @DatabaseField
    private String name;

    @DatabaseField
    private int level = 0;

    @DatabaseField
    private int militaryPoints = 0;

    @DatabaseField
    private int minePoints = 0;

    @DatabaseField
    private int civilPoints = 0;

    @DatabaseField
    private int points = 0;

    @DatabaseField
    private int pointsAll = 0;

    @DatabaseField
    private int money = 0;

    // Base
    @DatabaseField
    private int baseX = 0;
    @DatabaseField
    private int baseY = 0;
    @DatabaseField
    private int baseZ = 0;


    // Spawn
    @DatabaseField
    private int spawnX = 0;
    @DatabaseField
    private int spawnY = 0;
    @DatabaseField
    private int spawnZ = 0;


    // Stats
    @DatabaseField
    private int playerKills = 0;
    @DatabaseField
    private int playerDeaths = 0;
    @DatabaseField
    private int monsterKills = 0;
    @DatabaseField
    private int monsterDeaths = 0;
    @DatabaseField
    private int blockBreak = 0;
    @DatabaseField
    private int blockPlace = 0;
    @DatabaseField
    private int ironCollected = 0;
    @DatabaseField
    private int goldCollected = 0;
    @DatabaseField
    private int diamondCollected = 0;


    private ArrayList<Member> members = new ArrayList<Member>();
    private ArrayList<Member> invitedMembers = new ArrayList<Member>();


    private KingdomMessageChannel messageChannel = new KingdomMessageChannel();
    private KingdomStaffMessageChannel staffMessageChannel = new KingdomStaffMessageChannel();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KingdomLevel getLevel() {
        return Config.getLevel(level);
    }

    public void setLevel(int level) {
        this.level = level;
    }


    public int getMilitaryPoints() {
        return militaryPoints;
    }

    public void setMilitaryPoints(int military_points) {
        this.militaryPoints = military_points;
    }

    public void addMilitaryPoints( int points ) {
        this.militaryPoints += points;
        this.pointsAll += points;
    }

    public void removeMilitaryPoints( int points ) {
        this.militaryPoints -= points;
    }

    public int getMinePoints() {
        return minePoints;
    }

    public void setMinePoints(int mine_points) {
        this.minePoints = mine_points;
    }

    public void addMinePoints( int points ) {
        this.minePoints += points;
        this.pointsAll += points;
    }

    public void removeMinePoints( int points ) {
        this.minePoints -= points;
    }

    public int getCivilPoints() {
        return civilPoints;
    }

    public void setCivilPoints(int civil_points) {
        this.civilPoints = civil_points;
    }

    public void addCivilPoints( int points ) {
        this.civilPoints += points;
        this.pointsAll += points;
    }

    public void removeCivilPoints( int points ) {
        this.civilPoints -= points;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints( int points ) {
        this.points += points;
        this.pointsAll += points;
    }

    public void removePoints( int points ) {
        this.points -= points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getPointsAll() {
        return pointsAll;
    }

    public void setPointsAll(int pointsAll) {
        this.pointsAll = pointsAll;
    }

    public int getMoney() {
        return money;
    }

    public void addMoney( int amount ) {
        this.money += amount;
    }

    public void removeMoney( int amount ) {
        this.money -= amount;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Location getBaseLocation() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getBaseX(), getBaseY(), getBaseZ());
    }

    public Vector3d getBasePosition() {
        return getBaseLocation().getPosition();
    }

    public int getBaseX() {
        return baseX;
    }

    public void setBaseX(int baseX) {
        this.baseX = baseX;
    }

    public int getBaseY() {
        return baseY;
    }

    public void setBaseY(int baseY) {
        this.baseY = baseY;
    }

    public int getBaseZ() {
        return baseZ;
    }

    public void setBaseZ(int baseZ) {
        this.baseZ = baseZ;
    }

    public Location getBaseCornerOne() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getBaseX() - getLevel().getSize(), getBaseY(), getBaseZ() - getLevel().getSize());
    }

    public Location getBaseCornerTwo() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getBaseX() + getLevel().getSize(), getBaseY(), getBaseZ() + getLevel().getSize());
    }

    public int getSpawnX() {
        return spawnX;
    }

    public void setSpawnX(int spawnX) {
        this.spawnX = spawnX;
    }

    public int getSpawnY() {
        return spawnY;
    }

    public void setSpawnY(int spawnY) {
        this.spawnY = spawnY;
    }

    public int getSpawnZ() {
        return spawnZ;
    }

    public void setSpawnZ(int spawnZ) {
        this.spawnZ = spawnZ;
    }

    public Location getSpawnPoint() {
        return Sponge.getGame().getServer().getWorld("world").get().getLocation(getSpawnX(), getSpawnY(), getSpawnZ());
    }

    public int getPlayerKills() {
        return playerKills;
    }

    public void addPlayerKill() {
        this.playerKills++;
    }

    public void setPlayerKills(int playerKills) {
        this.playerKills = playerKills;
    }

    public int getPlayerDeaths() {
        return playerDeaths;
    }

    public void addPlayerDeath() {
        this.playerDeaths++;
    }

    public void setPlayerDeaths(int playerDeaths) {
        this.playerDeaths = playerDeaths;
    }

    public int getMonsterKills() {
        return monsterKills;
    }

    public void addMonsterKill() {
        this.monsterKills++;
    }

    public void setMonsterKills(int monsterKills) {
        this.monsterKills = monsterKills;
    }

    public int getMonsterDeaths() {
        return monsterDeaths;
    }

    public void addMonsterDeath() {
        this.monsterDeaths++;
    }

    public void setMonsterDeaths(int monsterDeaths) {
        this.monsterDeaths = monsterDeaths;
    }

    public int getBlockBreak() {
        return blockBreak;
    }

    public void addBlockBreak() {
        this.blockBreak++;
    }

    public void setBlockBreak(int blockBreak) {
        this.blockBreak = blockBreak;
    }

    public int getBlockPlace() {
        return blockPlace;
    }

    public void addBlockPlace() {
        this.blockPlace++;
    }

    public void setBlockPlace(int blockPlace) {
        this.blockPlace = blockPlace;
    }

    public ArrayList<Member> getMembers() {
        return members;
    }

    public void addMember( Member member ) {
        this.members.add(member);
    }

    public void removeMember( Member member ) {
        this.members.remove(member);
    }

    public void setMembers(ArrayList<Member> members) {
        this.members = members;
    }

    public ArrayList<Member> getInvitedMembers() {
        return invitedMembers;
    }

    public void addInvitedMember( Member member ) {
        this.invitedMembers.add(member);
    }

    public void removeInvitedMember( Member member ) {
        this.invitedMembers.remove(member);
    }

    public void setInvitedMembers(ArrayList<Member> invitedMembers) {
        this.invitedMembers = this.invitedMembers;
    }

    public boolean contains(Location location) {

        if ( baseX == 0 && baseY == 0 && baseZ == 0 ) {
            return false;
        }

        if ( getBaseCornerTwo().getBlockX() > location.getBlockX() && getBaseCornerOne().getBlockX() < location.getBlockX()) {

            if ( getBaseCornerTwo().getBlockZ() > location.getBlockZ() && getBaseCornerOne().getBlockZ() < location.getBlockZ() ) {
                return true;
            }

        }

        return false;
    }

    public ArrayList<Member> getOnlineMembers() {

        ArrayList<Member> onlineMembers = new ArrayList<Member>();

        for ( Member member : members ) {

            if ( member.isOnline() ) {
                onlineMembers.add(member);
            }

        }

        if ( onlineMembers.size() > 0 ) {
            return onlineMembers;
        }

        return null;
    }

    public void sendMessageToMembers(Text message) {
        messageChannel.send(message);
    }

    public void sendMessageToStaff(Text message) {
        staffMessageChannel.send(message);
    }

    public KingdomMessageChannel getMessageChannel() {
        return messageChannel;
    }

    public KingdomStaffMessageChannel getStaffMessageChannel() {
        return staffMessageChannel;
    }

    public int getIronCollected() {
        return ironCollected;
    }

    public void setIronCollected(int ironCollected) {
        this.ironCollected = ironCollected;
    }

    public void addIronCollected() {
        ironCollected++;
    }

    public int getGoldCollected() {
        return goldCollected;
    }

    public void setGoldCollected(int goldCollected) {
        this.goldCollected = goldCollected;
    }

    public void addGoldCollected() {
        goldCollected++;
    }

    public int getDiamondCollected() {
        return diamondCollected;
    }

    public void setDiamondCollected(int diamondCollected) {
        this.diamondCollected = diamondCollected;
    }

    public void addDiamondCollected() {
        diamondCollected++;
    }

    public void addToMessageChannels(Member member, Player player) {

        getMessageChannel().addMember(player);

        if (member.getRank() == KingdomRank.LEADER || member.getRank() == KingdomRank.CAPTAIN) {
            getStaffMessageChannel().addMember(player);
        }

    }

    public void sendTitleToMembers(Title title) {

        for (Member member: getOnlineMembers()) {
            Player player = Sponge.getServer().getPlayer(member.getUniqueId()).get();
            player.sendTitle(title);
        }

    }

}
