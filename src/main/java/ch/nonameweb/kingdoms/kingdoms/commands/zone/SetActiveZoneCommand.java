package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SetActiveZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        String name = args.<String>getOne(Text.of("name")).get();
        Boolean value = args.<Boolean>getOne(Text.of("value")).get();

        Zone zone = getZoneManager().getZoneByName( name );

        if ( zone == null ) {
            src.sendMessage(Text.of(TextColors.RED, "There is no Zone with this Name!"));
            return CommandResult.success();
        }

        if (zone.getFlagX() == 0 && zone.getFlagY() == 0 && zone.getFlagZ() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Flag is not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setflag"));
            return CommandResult.success();
        }

        if (zone.getCost() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Cost is not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setcost"));
            return CommandResult.success();
        }

        if (zone.getPoints() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Points is not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setpoints"));
            return CommandResult.success();
        }

        if (zone.getLifePoolDefenders() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Lifepool Defenders not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setlifepool"));
            return CommandResult.success();
        }

        if (zone.getLifePoolAttackers() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Lifepool Attackers not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setlifepool"));
            return CommandResult.success();
        }

        if (zone.getMinDefenders() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "min Defenders not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setminplayers"));
            return CommandResult.success();
        }

        if (zone.getMinAttackers() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "min Attackers not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setminplayers"));
            return CommandResult.success();
        }

        if (zone.getDelay() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Delay not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setdelay"));
            return CommandResult.success();
        }

        if (zone.getDuration() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Duration not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setduration"));
            return CommandResult.success();
        }

        if (zone.getCooldown() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Cooldown not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setcooldown"));
            return CommandResult.success();
        }

        if (zone.getCaptureTime() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Capture Time not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setcapturetime"));
            return CommandResult.success();
        }

        if (zone.getSpawnAttackersX() == 0 && zone.getSpawnAttackersY() == 0 && zone.getSpawnAttackersZ() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Spawn from Attackers is not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setspwan"));
            return CommandResult.success();
        }

        if (zone.getSpawnDefendersX() == 0 && zone.getSpawnDefendersY() == 0 && zone.getSpawnDefendersZ() == 0) {
            src.sendMessage(Text.of(TextColors.RED, "Spawn from Defenders is not set"));
            src.sendMessage(Text.of(TextColors.RED, "/zone setspwan"));
            return CommandResult.success();
        }

        zone.setActive( value );
        src.sendMessage(Text.of(TextColors.GOLD, "Zone Property is set!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.setactive")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.bool(Text.of("value"))
                )
                .executor(new SetActiveZoneCommand())
                .build();
    }

}