package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class LeaveKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            src.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if ( srcMember.getRank().equals( KingdomRank.LEADER ) ) {

            if ( getKingdomManager().getMembersFromKingdomByRank( srcKingdom, KingdomRank.LEADER ).size() == 1 ) {

                if ( srcKingdom.getMembers().size() > 1 ) {
                    src.sendMessage(Text.of(TextColors.RED, "You need to Promote first a Player to a Leader!"));
                    return CommandResult.success();
                }

            }

        }

        srcMember.setRank( KingdomRank.NONE );
        srcMember.setKingdom(0);

        if ( srcKingdom.getMembers().size() > 1 ) {

            //getPlotManager.removeOwnerFromPlots(member);
            srcKingdom.removeMember(srcMember);

            src.sendMessage(Text.of(TextColors.GOLD, "You have leaved the Kingdom!"));
            srcKingdom.sendMessageToMembers(Text.of(TextColors.RED, srcMember.getName(), " has left the Kingdom"));

        } else {
            getKingdomManager().deleteKingdom(srcKingdom);
            src.sendMessage(Text.of(TextColors.GOLD, "You have left and deleted the Kingdom!"));
        }

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.leave")
                .executor(new LeaveKingdomCommand())
                .build();
    }
}
