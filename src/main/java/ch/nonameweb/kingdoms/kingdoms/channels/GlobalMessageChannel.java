package ch.nonameweb.kingdoms.kingdoms.channels;

import ch.nonameweb.kingdoms.kingdoms.Config;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class GlobalMessageChannel extends BaseMessageChannel {

    public GlobalMessageChannel() {
        setPrefix(Text.of(TextColors.BLUE, Config.confNode.getNode("Settings", "Chat", "Global", "Prefix").getString()));
    }

}
