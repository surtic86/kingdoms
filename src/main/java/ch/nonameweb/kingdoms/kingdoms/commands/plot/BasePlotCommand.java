package ch.nonameweb.kingdoms.kingdoms.commands.plot;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class BasePlotCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Help"));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        if (srcKingdom != null) {

            if ( srcPlayer.hasPermission("kingdoms.command.plot.list") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "plot list -> lists all Plots"));
            }

            if ( srcPlayer.hasPermission("kingdoms.command.plot.info") ) {
                srcPlayer.sendMessage(Text.of(TextColors.GOLD, "plot info -> Infos about the Plot"));
            }

            if (
                srcMember.getRank().equals(KingdomRank.LEADER) ||
                srcMember.getRank().equals(KingdomRank.CAPTAIN)
            ) {

                if ( srcPlayer.hasPermission("kingdoms.command.plot.create") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "plot create <name>-> create a Plot from the Selection"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.plot.delete") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "plot delete -> delete the Plot"));
                }

                if ( srcPlayer.hasPermission("kingdoms.command.plot.setowner") ) {
                    srcPlayer.sendMessage(Text.of(TextColors.GOLD, "plot setowner <name> <player> -> set Owner of a Plot"));
                }

            }

        } else {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be in a Kingdom"));
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.plot")
                .child(CreatePlotCommand.build(), "create")
                .child(ListPlotCommand.build(), "list")
                .child(SetOwnerPlotCommand.build(), "setowner")
                .child(InfoPlotCommand.build(), "info")
                .child(DeletePlotCommand.build(), "delete", "remove")
                .executor(new BasePlotCommand())
                .build();
    }

}
