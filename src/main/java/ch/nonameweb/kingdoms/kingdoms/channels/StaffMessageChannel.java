package ch.nonameweb.kingdoms.kingdoms.channels;

import ch.nonameweb.kingdoms.kingdoms.Config;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class StaffMessageChannel extends BaseMessageChannel {

    public StaffMessageChannel() {
        setPrefix(Text.of(TextColors.DARK_RED, Config.confNode.getNode("Settings", "Chat", "Staff", "Prefix").getString()));
    }

}
