package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class SetSpawnKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "Du bist in keinem Königreich!"));
            return CommandResult.success();
        }

        if (!srcMember.getRank().equals(KingdomRank.LEADER)) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "Du musst ein Anführer sein um dies zu machen!"));
            return CommandResult.success();
        }

        if ( !srcKingdom.contains(srcPlayer.getLocation()) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "Du musst im Königreich stehen!"));
            return CommandResult.success();
        }

        srcKingdom.setSpawnX( srcPlayer.getLocation().getBlockX() );
        srcKingdom.setSpawnY( srcPlayer.getLocation().getBlockY() );
        srcKingdom.setSpawnZ( srcPlayer.getLocation().getBlockZ() );

        srcPlayer.sendMessage(Text.of(TextColors.GOLD, "Spawnpunkt erstellt!"));

        return CommandResult.success();
    }
    
    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.setspwan")
                .executor(new SetSpawnKingdomCommand())
                .build();
    }

}