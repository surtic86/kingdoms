package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class KickKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        if (
            !srcMember.getRank().equals( KingdomRank.LEADER ) &&
            !srcMember.getRank().equals( KingdomRank.CAPTAIN )
        ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You need to be a Captain ore Leader to do this!"));
            return CommandResult.success();
        }

        Player kickedPlayer = args.<Player>getOne("kickedPlayer").get();
        Member kickMember = getMemberManager().getMemberByPlayer(kickedPlayer);

        if ( kickMember == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "There are no Player with this Name!"));
            return CommandResult.success();
        }

        if ( srcMember.getKingdomName().equalsIgnoreCase( kickMember.getKingdomName() ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "The Player is not in your Kingdom!"));
            return CommandResult.success();
        }

        if ( srcMember.getRank().equals( kickMember.getRank() ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't kick a Member who has the same Rank!"));
            return CommandResult.success();
        }

        if ( kickMember.getRank().equals( KingdomRank.LEADER ) ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You can't kick a Leader!"));
            return CommandResult.success();
        }

        //getPlotManager.removeOwnerFromPlots(kickMember);
        srcKingdom.removeMember(kickMember);

        if (kickedPlayer.isOnline()) {
            kickedPlayer.sendMessage( Text.of(TextColors.RED , "You where kicked from Kingdom!") );
        }

        srcKingdom.sendMessageToMembers(Text.of(TextColors.GOLD ,kickedPlayer.getName(), " was kicked from Kingdom!"));

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.kick")
                .arguments(
                        GenericArguments.player(Text.of("kickedPlayer"))
                )
                .executor(new KickKingdomCommand())
                .build();
    }
}
