package ch.nonameweb.kingdoms.kingdoms.channels;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.channel.MutableMessageChannel;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class BaseMessageChannel implements MutableMessageChannel {

    protected Text prefix;
    protected Set<MessageReceiver> members;

    public BaseMessageChannel() {
        members = new HashSet<>();
    }

    public void setPrefix(Text prefix) {
        this.prefix = prefix;
    }

    @Override
    public boolean addMember(MessageReceiver member) {
        return members.add(member);
    }

    @Override
    public boolean removeMember(MessageReceiver member) {
        return members.remove(member);
    }

    @Override
    public void clearMembers() {
        members.clear();
    }

    @Override
    public Collection<MessageReceiver> getMembers() {
        return members;
    }

    @Override
    public Optional<Text> transformMessage(Object sender, MessageReceiver recipient, Text original, ChatType type) {
        Text text = original;
        text = Text.of(prefix, " ", TextColors.RESET, text);
        return Optional.of(text);
    }
}
