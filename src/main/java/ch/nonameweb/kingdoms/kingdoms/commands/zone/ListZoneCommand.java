package ch.nonameweb.kingdoms.kingdoms.commands.zone;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.Zone;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ListZoneCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "List all Zones"));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));


        for ( Zone zone : getZoneManager().getAllZones() ) {

            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "ID : ", TextColors.WHITE, zone.getId(), TextColors.YELLOW, " Name : ", TextColors.WHITE, zone.getName(), TextColors.YELLOW, " Active : ", TextColors.WHITE, zone.isActive()));
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Build : ", TextColors.WHITE, zone.isBuild(), TextColors.YELLOW, " Cost : ", TextColors.WHITE, zone.getCost(), TextColors.YELLOW, " Points per Hour : ", zone.getPoints()));
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Min. Online Defender : ", TextColors.WHITE, zone.getMinDefenders(), TextColors.YELLOW, " Attackers : ", TextColors.WHITE, zone.getMinAttackers()));
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Life Pool Defender : ", TextColors.WHITE, zone.getLifePoolDefenders(), TextColors.YELLOW, " Attackers : ", TextColors.WHITE, zone.getLifePoolAttackers()));
            srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Delay : ", TextColors.WHITE, zone.getDelay(), TextColors.YELLOW, "sec. Name : ", TextColors.WHITE, zone.getDuration(), TextColors.YELLOW, "sec. Cooldown : ", zone.getCooldown(), "sec"));

            srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        }

        return CommandResult.success();
    }

    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.zone.list")
                .executor(new ListZoneCommand())
                .build();
    }

}