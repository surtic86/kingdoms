package ch.nonameweb.kingdoms.kingdoms.entity;

public enum KingdomRank {
    LEADER,
    CAPTAIN,
    MEMBER,
    NOVICE,
    NONE
}

