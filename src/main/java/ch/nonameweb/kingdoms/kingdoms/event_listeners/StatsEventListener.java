package ch.nonameweb.kingdoms.kingdoms.event_listeners;

import ch.nonameweb.kingdoms.kingdoms.Kingdoms;
import ch.nonameweb.kingdoms.kingdoms.entity.Kingdom;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import ch.nonameweb.kingdoms.kingdoms.manager.KingdomManager;
import ch.nonameweb.kingdoms.kingdoms.manager.MemberManager;
import ch.nonameweb.kingdoms.kingdoms.manager.PlotManager;
import ch.nonameweb.kingdoms.kingdoms.manager.ZoneManager;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.monster.Monster;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.filter.IsCancelled;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.util.Tristate;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class StatsEventListener {

    private Kingdoms plugin;
    private MemberManager memberManager;
    private KingdomManager kingdomManager;
    private PlotManager plotManager;
    private ZoneManager zoneManager;

    public StatsEventListener() {
        plugin = Kingdoms.getInstance();
        memberManager = plugin.getMemberManager();
        kingdomManager = plugin.getKingdomManager();
        plotManager = plugin.getPlotManager();
        zoneManager = plugin.getZoneManager();
    }


    public void addToMessageChannels(Player player, Member member) {

        plugin.getLocalMessageChannel().addMember(player);
        plugin.getGlobalChannel().addMember(player);

        if (player.hasPermission("kingdoms.chat.staff")) {
            plugin.getStaffMessageChannel().addMember(player);
        }

        Kingdom kingdom = kingdomManager.getKingdomByPlayer(player);

        if (kingdom != null) {
            kingdom.addToMessageChannels(member, player);
        }

    }

    @Listener
    public void onPlayerJoin(ClientConnectionEvent.Join event, @Root Player player) {

        Member member = memberManager.getMemberByPlayer(player);

        if ( member == null ) {
            member = memberManager.createMember(player);
        }

        member.setOnline(true);
        member.setLastLogin( new Date(System.currentTimeMillis() ) );
        member.updateMessageChannel();
        member.setPlayerKingdomTag();
        memberManager.save(player);

        addToMessageChannels(player, member);
    }

    @Listener
    public void onPlayerQuitEvent(ClientConnectionEvent.Disconnect event, @Root Player player) {

        Member member = memberManager.getMemberByPlayer(player);

        if ( member == null ) {
            member = memberManager.createMember(player);
        }

        member.setOnline(false);
        member.setLastLogout( new Date(System.currentTimeMillis()) );
        memberManager.save(player);
    }

    @Listener(order = Order.LAST)
    @IsCancelled(Tristate.FALSE)
    public void onChangeBlockEventBreak(ChangeBlockEvent.Break event, @Root Player player) {

        Member member = memberManager.getMemberByPlayer(player);

        List<Transaction<BlockSnapshot>> transactions = event.getTransactions();

        for ( Transaction<BlockSnapshot> transaction : transactions ) {

            if (transaction.getOriginal().getState().getType().equals(BlockTypes.DIAMOND_ORE)) {
                member.addDiamondCollected();

            } else if (transaction.getOriginal().getState().getType().equals(BlockTypes.GOLD_ORE)) {
                member.addGoldCollected();

            } else if (transaction.getOriginal().getState().getType().equals(BlockTypes.IRON_ORE)) {
                member.addIronCollected();

            }

            member.addBlockBreak();
        }

        Kingdom kingdom = kingdomManager.getKingdomByPlayer(player);
        if (kingdom != null) {

            for ( Transaction<BlockSnapshot> transaction : transactions ) {

                if (transaction.getOriginal().getState().getType().equals(BlockTypes.DIAMOND_ORE)) {
                    member.addDiamondCollected();

                } else if (transaction.getOriginal().getState().getType().equals(BlockTypes.GOLD_ORE)) {
                    member.addGoldCollected();

                } else if (transaction.getOriginal().getState().getType().equals(BlockTypes.IRON_ORE)) {
                    member.addIronCollected();

                }

                kingdom.addBlockBreak();
            }

        }

    }

    @Listener(order = Order.LAST)
    @IsCancelled(Tristate.FALSE)
    public void onChangeBlockEventPlace(ChangeBlockEvent.Place event, @Root Player player) {

        Member member = memberManager.getMemberByPlayer(player);
        member.addBlockPlace();

        Kingdom kingdom = kingdomManager.getKingdomByPlayer(player);
        if (kingdom != null) {
            kingdom.addBlockPlace();
        }

    }

    @Listener(order = Order.LAST)
    @IsCancelled(Tristate.FALSE)
    public void onEntityDeath(DestructEntityEvent.Death event) {

        Optional<EntityDamageSource> optDamageSource = event.getCause().first(EntityDamageSource.class);

        if (optDamageSource.isPresent()) {

            EntityDamageSource damageSource = optDamageSource.get();
            Entity killer = damageSource.getSource();

            if (killer instanceof Player) {

                Member killerMember = memberManager.getMemberByPlayer((Player)killer);
                Kingdom killerKingdom = killerMember.getKingdom();

                if (event.getTargetEntity() instanceof Player) {

                    killerMember.addPlayerKill();
                    if (killerKingdom != null) {
                        killerKingdom.addPlayerKill();
                    }

                    Member killedMember = memberManager.getMemberByPlayer((Player)event.getTargetEntity());
                    Kingdom killedKingdom = killedMember.getKingdom();

                    killedMember.addPlayerDeath();
                    if (killedKingdom != null) {
                        killedKingdom.addPlayerDeath();
                    }

                } else if (event.getTargetEntity() instanceof Monster) {

                    killerMember.addMonsterKill();
                    if (killerKingdom != null) {
                        killerKingdom.addMonsterKill();
                    }

                }

            } else if (killer instanceof Monster) {

                if (event.getTargetEntity() instanceof Player) {

                    Member killedMember = memberManager.getMemberByPlayer((Player)event.getTargetEntity());
                    Kingdom killedKingdom = killedMember.getKingdom();

                    killedMember.addMonsterDeath();
                    if (killedKingdom != null) {
                        killedKingdom.addMonsterDeath();
                    }

                }

            }

        }

    }

}
