package ch.nonameweb.kingdoms.kingdoms.commands.kingdom;

import ch.nonameweb.kingdoms.kingdoms.commands.BaseCommand;
import ch.nonameweb.kingdoms.kingdoms.entity.KingdomRank;
import ch.nonameweb.kingdoms.kingdoms.entity.Member;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;

public class MembersKingdomCommand extends BaseCommand implements CommandExecutor {

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        initSrcPlayer(src);

        if (srcPlayer == null) {
            src.sendMessage(Text.of("You need to be a Player!"));
            return CommandResult.success();
        }

        if ( srcKingdom == null ) {
            srcPlayer.sendMessage(Text.of(TextColors.RED, "You are not in a Kingdom!"));
            return CommandResult.success();
        }

        ArrayList<Member> leaders = getKingdomManager().getMembersFromKingdomByRank(srcKingdom, KingdomRank.LEADER);
        ArrayList<Member> captains = getKingdomManager().getMembersFromKingdomByRank(srcKingdom, KingdomRank.CAPTAIN);
        ArrayList<Member> members = getKingdomManager().getMembersFromKingdomByRank(srcKingdom, KingdomRank.MEMBER);
        ArrayList<Member> novices = getKingdomManager().getMembersFromKingdomByRank(srcKingdom, KingdomRank.NOVICE);

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Members [" + srcKingdom.getTag() + "] " + srcKingdom.getName()));
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));
        srcPlayer.sendMessage(Text.of(TextColors.YELLOW, "Name - Kills - Deaths"));

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "Leaders"));
        sendMemberGroupInfos(srcPlayer, leaders);
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "Captains"));
        sendMemberGroupInfos(srcPlayer, captains);
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "Members"));
        sendMemberGroupInfos(srcPlayer, members);
        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "Novices"));
        sendMemberGroupInfos(srcPlayer, novices);

        srcPlayer.sendMessage(Text.of(TextColors.BLUE, "-----------------------------------------------------"));

        return CommandResult.success();
    }


    public void sendMemberGroupInfos(Player player, ArrayList<Member> members) {

        if ( members == null || members.size() < 1) {
            return;
        }

        for ( Member member : members ) {

            TextColor color = TextColors.WHITE;

            if ( member.isOnline() ) {
                color = TextColors.AQUA;
            }

            player.sendMessage(Text.of(color, member.getName(), " ", member.getPlayerKills(), " ", member.getPlayerDeaths()));
        }

    }

    
    public static CommandSpec build() {
        return CommandSpec.builder()
                .permission("kingdoms.command.kingdom.members")
                .executor(new MembersKingdomCommand())
                .build();
    }

}